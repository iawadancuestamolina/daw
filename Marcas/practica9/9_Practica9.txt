<!DOCTYPE html>

<!-- 
-->
<HTML>
	<head> 
	
		<META Charset="UTF-8"> <!-- o també unicode-->
		<title> Taula amb imatges</title>
	</head>
	
	<body style="background-color: aqua;"> <!-- Per colors nomes es posa bgcolor-->
	
		<H1> Taula amb imatges:</H1>
		
		< style="background-image:url(img/Mark_Knopfler.jpg); border: 5px solid green; width:80%;">
		
			<>
				< style="width:60%; height:110px; text-align:center; background-color:red; border:3px solid yellow; color: white;">text alineat al centre verticalment i a l'esquerra horitzontalment </td>
				< style="width:30%; height:110px; text-align:center; color: white">text alineat al centre verticalment i a l'esquerra horitzontalment</td>
				< style="background-color: yellow; width:10%; border:8px solid white;"> Yellow </td>
									
			</tr>
			
			<>
				<style="width:60%; height:110px; text-align:right; vertical-align:top; color: white;"> text alineat a dalt verticalment i la dreta horitzontalment</td>
				< style="width:30%; height:110px; vertical-align:top; text-align:right; color: aqua; background-image: url(img/747-200.jpg);"> text alineat a dalt verticalment i la dreta horitzontalment</td>
				<style="background-color: red; width:'10%'; color: white;">red </td>
													
			</>
			
			<style="background-image:url(img/DSCN3964.JPG);"> <!-- El background es per el fons d'un element-->
				<style="width:60%; height:110px; vertical-align: bottom; color: white;"> text alineat a baix verticalment i a l'esquerra horitzontalment</td>
				<style="width:30%; height:110px; vertical-align: bottom; color: white;">text alineat a baix verticalment i a l'esquerra horitzontalment</td>
				<style="background-color: purple; width: 10%; color: white;"> Purple </td>
									
			</>
			
			<>
				<style="width:60%; height:110px; padding: 50px; color: white;"> text alineat </td>
				<style="width:30%; height:110px; padding: 50px; color: white; background-image: url(img/Kill_to_Get_Crimson_Cover.jpg);"> text alineat </td>
				<style="background-color: lime; width:10%;border: 5px solid red; color: white;"> Lime </td>	
			</>
						
	</body>
	
</HTML>
