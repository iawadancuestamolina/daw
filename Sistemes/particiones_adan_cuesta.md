### Exercicis del LPI 104.2-104.3

*Busqueu als enllaços corresponents que teniu al moodle les respostes a les següents preguntes:*

##### Exercici 1

Enumereu quina és la informació referent a un fitxer que conté l'estructura anomenada inode.(104.2)
```
Un inode almacena información acerca del autor del archivo, los permisos o la última vez que se accedió a el.
```

##### Exercici 2.

L'inode conté el nom del fitxer? Quan executem alguna de les ordres al bash
que ens proporciona algun tipus d'informació d'un fitxer, com s'ho fa el kernel
de linux per arribar a aquesta informació? *104.2*

**Resposta**: el inode no contiene el nombre y no tengo ni idea de lo otro.

##### Exercici 3.

Com es diu a la propietat que tenen alguns sistemes de fitxers per poder implementar transaccions i poder restablir les dades del sistema de fitxers en cas de corrupció d'aquestes dades (com es fa a una base de dades). *104.2*

**Resposta:** Integridad

##### Exercici 4.
   
Mostreu en ordre els directoris que ocupen més espai del vostre directori i subdirectoris. *104.2*

**Resposta:**
```
du -d 1 /home/fanelli | sort -n -r
```

##### Exercici 5. 

Mostreu els fitxers que ocupen al menys un *MB* al vostre *HOME* *104.2*

**Resposta:**
```
du -h -t 1M /home/fanelli
```

##### Exercici 6. 

O si ho teniu en castellà (amb coma enlloc de punt):

**Resposta:** ?????

##### Exercici 7. 
	
Mostreu ara un resum *humà*, o sigui el total ocupat al nostre directori. *104.2*

```
du -hc /home/fanelli
```

##### Exercici 8. 

Quina ordre ens mostrarà l'espai lliure i ocupat de totes les particions que
tinguem en format *llegible per humans*? I si volguéssim que al mesurar les
particions s'utilitzés que *1k* en comptes de ser 2¹⁰ (o sigui 1024) fos 10³ (o
sigui 1000), ja que hi ha alguns fabricants que ho fan d'aquesta manera,
sistema internacional, i així ens poden *vendre* menys espai del que ens
pensàvem ? *104.2*

**Resposta:** 
```
df -H -h
```

##### Exercici 9. 

Si hi ha una caiguda de tensió i s'apaga/reinicia el S.O., hi ha alguna eina
que ens ajudi a solucionar el possible problema d'inconsistència de dades? (Ens
referim a que hi hagi dades que no s'hagi acabat d'escriure a disc)  *104.2*

**Resposta:**
```
fsck
```

##### Exercici 10. 
	
Suposem que volem que a l'arrancar el sistema es faci un xequeig automàtic
d'una partició on tenim un sistema de fitxers *ext4* (per exemple a `/dev/sda6`).
Com ho faràs? Recorda `man ordre/fitxer` *104.3*

**Resposta:**
Hem de modificar el fitxer /etc/fstab i posar un 2 a l'ultime columne de la fila de la partició

##### Exercici 11. 

De vegades intentem desmuntar un dispositiu i el sistema no en deixa perquè diu
que el dispositiu està ocupat. Amb quina ordre podria saber quins són els
fitxers oberts o quin procés té fitxers oberts? *104.3* 

**Resposta:** Puedes usar la orden `lsof` o la orden `fuser`


