### Exercicis fets amb Fedora 27 (octubre 2018)


##### Exercici 1.
Responeu a la pregunta sense provar-ho a la terminal: què es mostrarà per pantalla si executo:

```
[leviatan@pc666 ~]$ echo -n "modul 01" || echo "UF1" && echo -e "\nSistemes Informatics"
```
**Resposta:** El que sucederá es que es  mostrará per pantalla "modul 01" i a sota "Sistemes Informatics"  per que hem donat a escollir al ordinador entre el primer echo i el segon i l'hem dit que si la primera ordre no dona error que ejecute el tercer echo.


##### Exercici 2.
Executa només la primera ordre i endevina que mostrarà la segona:
```
echo $BASH_SUBSHELL;   
(sleep 1; echo $BASH_SUBSHELL; sleep 1)
```
**Resposta:** La segona ordre mostrará el nombre 1, en referència al nivell de bash al que ens encontrem (parèntesis).


##### Exercici 3.
Feu el listing 9 afegint una ordre "sleep 10" al final, de manera que es llegeixi:

```
[leviatan@pc666 ~]$ bash -c "echo Expand in parent $$ $PPID; sleep 10"
```
Abans de que no s'esgotin els 10 segons executeu en una altra terminal la següent comanda:
```
pstree -pha
```
Mireu de trobar el procés *sleep*

**Resposta: ** Creo que está aquí:
```
gnome-terminal-,26596
  │   │   ├─bash,26660
  │   │   │   └─bash,27646 -c echo Expand in parent 26660 26596; sleep 10
  │   │   │       └─sleep,27647 10
```

##### Exercici 4.
Estic al bash i he executat:
```
[leviatan@pc666 ~]$ animal="cat"
```
o potser
```
[leviatan@pc666 ~]$ animal="dog"
```
de fet no ho recordo bé, però vull que em mostri per pantalla el plural en anglès de l'animal que hi hagi a la variable.
Quina ordre executaries?

**Resposta: ** el comando es echo "$animal"s

##### Exercici 5.
Si obro una terminal i escric:
```
[leviatan@pc666 ~]$ exec sleep 10
```
que succeeix? ho podries explicar?

**Resposta:** En teoría el comando exec ejecuta un programa y sustituye la ejecución del  bash por la ejecución de ese programa. En este caso en lugar de un programa es una orden.

*Històric*
##### Exercici 6.
Dona una ordre amb la qual pugui veure la meva arquitectura (bé, de fet la del meu pc). I la del nucli? i si vull una info completa del sistema (un breu resum)

**Resposta: ** uname -m; uname -r; uname -a

##### Exercici 7.
Quantes línies es desen a l'històric de comandes a memòria ? Troba la variable d'entorn que conté aquesta informació, o sigui el número de línies de l'històric a memòria. (hint: history, apropos, help ...)

**Resposta:** La variable es $HISTSIZE i en el meu cas hi son 1000 línies las que es desen

##### Exercici 8. 
I al fitxer històric de comandes ? Troba la variable d'entorn que conté aquesta informació (el número de línies del fitxer històric).

**Resposta:** La variable que conté l'informació no és una variable d'entorn com $HISTSIZE, per que no apareix si fas un *env*, pèro aquesta informació es pot trobar à la variable $HISTFILESIZE

##### Exercici 9. 
Es desen les ordres consecutives duplicades a l'històric de comandes? Quina variable conté aquesta informació? Doneu una solució fàcil perquè sí que emmagatzemi les ordres consecutives repetides.
Intenteu esbrinar a quin fitxer es troba aquesta variable.  

**Resposta:** En el meu cas no es desen per que la variable $HISTCONTROL te el valor *ignoredups*, si volem que sí que es desen las ordres duplicades canviarem el valor d'aquesta variable a *ignorespace*. Esta variable s'enmagatzema al fitxer ~/.bashrc

##### Exercici 10. 
Amb quina combinació de tecles faig una cerca inversa i puc trobar la darrera vegada que he utilitzat una ordre que conté un cert patró? Com trobo l'anterior ocurrència?

**Resposta:**  Amb Ctrl+r es pot utilitzar la cerca inversa teclejant el patró que vulgis. Et pots moure al l'ordre previa amb Ctrl+p o a la següent amb Ctrl+n.

##### Exercici 11. 
En aquest exercici no heu de resoldre res, només executar les ordres que s’enumeren, però si no feu el següent exercici que resol el que heu fet en aquest, no podreu accedir al programa java de veritat i per tant el DrJava que utilitza aquesta ordre java no funcionarà.

Feu:
```
echo $PATH
```
Se us mostren una sèrie de directoris separats pel caràcter ":"

Per exemple una sortida podria ser:
```
/usr/local/bin:/usr/bin:/bin:/usr/local/sbin:/usr/sbin:/home/leviatan/.local/bin:/home/leviatan/bin
```
*ALERTA: Aquest exercici només funciona si al 1er directori del vostre PATH no es troba un executable de nom java.*

Com a root fem el següent:
```
[root@pc666 ~]# vim /usr/local/bin/java
```
i dintre escrivim les següents línies:
```
#!/bin/bash
# Script que mostra la data
date
```
Tanquem i desem el fitxer. Com a root donem permís d'execució:
```
[leviatan@pc666 ~]$ chmod +x /usr/local/bin/java
```
I ara tornem a l'usuari ordinari iawxxxxxx i executeu:
```
[iawxxxxxx@pc666 ~]$ java
```

**Resposta:** Parece que en el único ejercicio que no hay que hacer nada no soy capaz de resolverlo, me dice: "/usr/local/bin/java"
"/usr/local/bin/java" E212: No se pudo abrir el archivo para escribir en él
He seguido los pasos al pié de la letra varias veces...

##### Exercici 12.
Arreglem el problema creat abans.

**Resposta:** No puedo, porque no he podido ejecutar los comandos anteriores

##### Exercici 13.
Ara treballaré amb un àlies (alerta amb les cometes _no escapades_):
```
[leviatan@pc666 ~]$ alias java="echo Java és una illa d\'Indonèsia"
```
executeu ara:
```
[iawxxxxxx@pc666 ~]$ java
```
És necessari eliminar aquest alies ? Tant si ho és com si no, digues com s'elimina un alies:

Si vull que un àlies funcioni sempre que hauria de fer?

**Resposta:** Si vols que java funcioni correctament haurás de eliminar aquet alies amb *unalias java*. Si vols que duri ho pots deixar tal qual.

##### Exercici 14.
Suposem que volem instal·lar-nos el CD de F27 64 bits net-install des d'[aquest enllaç](https://archives.fedoraproject.org/pub/archive/fedora/linux/releases/27/Workstation/x86_64/iso/Fedora-Workstation-netinst-x86_64-27-1.6.iso) i un cop hem baixat la iso volem asegurar-nos que ha estat ben baixada.

Al mateix directori que conté la iso trobem el fitxer:

Fedora-Workstation-27-1.6-x86_64-CHECKSUM

i dintre la línia:
```
SHA256 (Fedora-Workstation-netinst-x86_64-27-1.6.iso) = 18ef4a6f9f470b40bd0cdf21e6c8f5c43c28e3a2200dcc8578ec9da25a6b376b
```
El nostre sistema tindra alguna ordre relacionada amb aquest SHA256 per calcular el checksum

Investigueu amb quina ordre podem calcular aquesta cadena per veure si ens hem baixat be la iso. (hint: apropos)

**Resposta:** Desprès de molt investigar crec que l'ordre que pot fer el checksum de aquesta iso es *sha256sum*


