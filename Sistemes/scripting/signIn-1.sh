#!/bin/bash	
# Filename:			signin.sh
# Author:			Adán Cuesta Molina
# Date:				8/3/2021
# Version:			0.1
# License:			This is free software, licensed under the GNU General Public License v3.
# 					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./mostra_arguments.sh  
# Description: 		Realitza un script per recrear un sign in
# 
# Cal demanar a l’usuari el seu e-mail. Exemple@foo.bar
# L’script ha de comprovar que el format és correcte:

# Usuari: lletres (majúscules i minúscules), números i 3 símbols ( _ - . ) barra baixa, menys i punt.
# El servidor *foo* lletres (majúscules i minúscules), números.
# El domini *bar* sols lletres minúscules.

# En cas de ser erroni el mail, avisem a l’usuari i sortim del programa.
# Un cop introduït el mail correctament, demanem a l’usuari una contrasenya alfanumèrica de mínim 8 
# caràcters i màxim 20. Utilitzeu expressions regulars.

# En cas de ser erroni el password, avisem a l’usuari i sortim del programa.
# Si és correcte demanem de nou que repeteixi el password i comprovem si son iguals.
# Si son iguals avisem que el compte s’ha creat amb èxit.
# Si son diferents avisem del registre incorrecte i sortim del programa.

# Give permisions to the script

chmod u+x signin.sh

# Pedimos la entrada de datos

echo -e "\nHola!
\nIntroduce tu direccion de email."
echo -e "\n"

mail="^[[:alnum:]_-.]+@[[:alnum:]]+\.[[:lower:]]+$"
# Aqui iniciamos las variables, ocultando el campo password por seguridad

read -p 'Dirección de Email : ' emailvar 

# Ahora chequeamos que la direccion de email es válida

if  [[ $emailvar =~ $mail ]] ;
then 
     read -sp 'Ahora introduce tu contraseña :' passvar 

if [[ $passvar != "[:anum:]{3,20}" ]] ;

        then echo "La password introducida no es válida"

        exit
fi

else
    
	echo "La dirección de Email no es correcta, ciao pescao!"
	exit
fi
# Ahora solicitamos passwords y verificamos el formato

echo -e "\n"

# Solicitamos verificación

read -sp 'Introduce de nuevo la contraseña :' passvar2

if [[ "$passvar" != "$passvar2" ]] ;

        then 
		echo "Las contraseñas no coinciden"

 	       exit
       

        else 
		echo "Tu cuenta se ha creado, bienvenido a tu tienda favorita, gasta mucho, por favor!"

fi
