

#!/bin/bash
# Filename:	calculadora.sh
# Author:	iaw50482126
# Date:	    04-12-20
# Version:	0.1
# License:  This is free software, licensed under the GNU General Public License v3.
#        See http://www.gnu.org/licenses/gpl.html for more information.	
# Usage:    calculadora.sh [arg1...]	
# Description: programa que usa bc y read para leer una serie de operaciones matemáticas, almacena la solución en una variable y la muestra por pantalla

