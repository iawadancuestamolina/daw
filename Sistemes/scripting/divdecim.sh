

#!/bin/bash
# Filename:	divdecim.sh
# Author:	fanelli
# Date:	    05-12-20
# Version:	0.1
# License:  This is free software, licensed under the GNU General Public License v3.
#        See http://www.gnu.org/licenses/gpl.html for more information.	
# Usage:    divdecim.sh [arg1...] [arg2...]	
# Description: División con decimales donde el primer argumento es el dividendo y el segundo es el divisor y tiene que hacer una división de números reales con dos decimales.

if test $# -ne 2 #hacemos test para ver si el número de argumentos ($#) no son dos (-ne "not equal")
then
		echo "Error: Tienes que escribir dos argumentos"
		echo -e "Uso:\t $0 num1 num2"
		exit 1 #salimos con salida de error 1
fi

resultado=$(echo "scale=2; $1/$2"|bc -l) 
#almacenamos en la variable resultado la orden echo de la división (2decimales) e introducimos estos datos por la entrada del comando bc

echo $resultado

# vim:ai:ts=4:sw=4:syntax=sh
