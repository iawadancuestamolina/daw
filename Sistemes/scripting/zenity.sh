

#!/bin/bash
# Filename:	zenity.sh
# Author:	fanelli
# Date:	    09-12-20
# Version:	0.1
# License:  This is free software, licensed under the GNU General Public License v3.
#        See http://www.gnu.org/licenses/gpl.html for more information.	
# Usage:    zenity.sh [arg1...]	
# Description: show a fortune window at the beginning of the session being graphical or not

if test -n $DISPLAY 
		#comprobamos con test que la variable display está vacía, si es así significa que estamos sin sesión gráfica
then
		mensaje=$(fortune)    #ejecutamos fortune dentro de la variable mensaje y almacenamos así la frase que nos de (también se podría hacer con --text="$(fortune)" y funcionaria mejor)
		zenity --info --text="$mensaje" title="fortune" --no-wrap & 
else
	fortune
fi
