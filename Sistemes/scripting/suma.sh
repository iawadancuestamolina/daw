

#!/bin/bash
# Filename:	suma.sh
# Author:	fanelli
# Date:	    03-12-20
# Version:	0.1
# License:  This is free software, licensed under the GNU General Public License v3.
#        See http://www.gnu.org/licenses/gpl.html for more information.	
# Usage:    suma [arg1...]	
# Description: script that gets from the command line 2 eint numbers add them and show the result. If it doesn't get the arguments show an error message and exits from the script with error level != 0
 
if test $# -ne 2
then 
	echo "error: número de argumentos incorrecto"
	echo -e "uso:\t $0 num1 num2"
	exit 1 #salimos del programa con error level 1
fi

#sumamos los dós números y los almacenamo
suma=$(($1+$2))

#mostramos valor almacenado
echo $suma


# vim:ai:ts=4:sw=4:syntax=sh
