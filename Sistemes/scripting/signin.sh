#!/bin/bash	
# Filename:			signin.sh
# Author:			Adán Cuesta Molina
# Date:				8/3/2021
# Version:			0.1
# License:			This is free software, licensed under the GNU General Public License v3.
# 					See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:			./mostra_arguments.sh  
# Description: 		Realitza un script per recrear un sign in
# 
# Cal demanar a l’usuari el seu e-mail. Exemple@foo.bar
# L’script ha de comprovar que el format és correcte:

# Usuari: lletres (majúscules i minúscules), números i 3 símbols ( _ - . ) barra baixa, menys i punt.
# El servidor *foo* lletres (majúscules i minúscules), números.
# El domini *bar* sols lletres minúscules.

# En cas de ser erroni el mail, avisem a l’usuari i sortim del programa.
# Un cop introduït el mail correctament, demanem a l’usuari una contrasenya alfanumèrica de mínim 8 
# caràcters i màxim 20. Utilitzeu expressions regulars.

# En cas de ser erroni el password, avisem a l’usuari i sortim del programa.
# Si és correcte demanem de nou que repeteixi el password i comprovem si son iguals.
# Si son iguals avisem que el compte s’ha creat amb èxit.
# Si son diferents avisem del registre incorrecte i sortim del programa.

# Give permisions to the script

chmod u+x signin.sh

#pedimos el mail
echo "Escribe el email:"
read email

#hacemos una variable con la expresión regular
expmail='^[[:alnum:]_.-]+@[[:alnum:]]+\.[[:lower:]]+$'
exppasswd='^[[:alnum:]]{8,20}$'

#ahora hacemos un if para comprobar el mail y la contraseña
if [[ $email =~ $expmail ]];
then
    echo "Escribe tu password:"
    read -s passwd
    if [[ $passwd =~ $exppasswd ]];#si es correcto el password pedimos que lo repita
    then
        echo "Introduce de nuevo el password:"
        read -s passwd2
        if [[ $passwd = $passwd2 ]];#comprobamos que el password coincide con el anterior
        then
            echo "Muy bien, cuenta creada!"
            exit
        else
            echo "Las constraseñas no coinciden, no se creará la cuenta!"
            exit
        fi
    else
        echo "Esa contraseña no vale!"
        exit    
    fi
else
    echo "Este email no es válido!"
    exit
fi