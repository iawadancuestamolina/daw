## Exercicis tema LPI 103.3 Tasques bàsiques d'administració de fitxers

##### Exercici 1.

Acabeu de treballar el listing 4 dels apunts de IBM suposant que teniu un fitxer qualsevol _text1_ que es troba al nostre `$HOME/iawxxxxx` de gandhi. Feu:

```
ls -i text1
```

Després:

```
mv text1 un_directori_de_gandhi/text2
ls -i un_directori_de_gandhi/text2
```

I finalment fer l'operació anterior però desant una còpia del nostre fitxer al pen:

```
mv  un_directori_de_gandhi/text2  un_directori_del_pen_usb/text2
ls -i un_directori_del_pen_usb/text2
``` 

Compareu els inodes que ens han sortit en els 3 casos i expliqueu aquest comportament?

**Resposta:** en los dos primeros casos aparecía el número 798 y en el último el número 99. Se trata de los diferentes números de archivo (inode) que sirven para identificar cada archivo.


Per acabar copieu un fitxer prou gran de _public_ a `/tmp`,com per exemple `/home/groups/inf/public/install/IDE/android/android-studio/android-studio-ide-141.2135290-linux.zip` i poseu davant de l'ordre `cp` l'ordre `time`.

Després moveu el mateix fitxer zip de `/tmp` a un subdirectori qualsevol de `/tmp`.
Triga el mateix temps? Perquè?

**Resposta:** Prácticamente tarda lo mismo 0.036s y 0.037s respectivamente.

---

_Utilitzeu l'ordre `find` per aconseguir el que es demana als següents exercicis:_

##### Exercici 2.

Llisteu tots els fitxers del directori actual que han estat modificats a l'últim dia.

**Resposta:** 

[fanelli@Macbook DAW]$ find -atime 1


##### Exercici 3.

Llisteu tots els fitxers del sistema que són més grans d'1 MB.

**Resposta:**

[fanelli@Macbook DAW]$ find / -size +1M


##### Exercici 4.

Elimineu tots els fitxers amb extensió "class" de l'estructura de directori que comença al vostre directori personal. Aquest exercici pot ser molt perillós, es recomana fer una còpia del directori a on teniu els fitxers de Java al directori `/tmp` i jugar a `/tmp`.

**Resposta:**

[fanelli@Macbook DAW]$ find . -name "*.txt" -exec rm '{}' \;


##### Exercici 5.

Llisteu els inodes de tots els fitxers java del directori actual.

**Resposta:**

[fanelli@Macbook DAW]$ find . -name "*.java" -exec ls -i '{}' \;


##### Exercici 6.

Llisteu tots els fitxers del sistema de fitxers local (per tant sense incloure els de gandhi ja que pertanyen a un altre sistema de fitxers) que hagin estat modificats a l'últim mes.

**Resposta:**

[fanelli@Macbook DAW]$ find / -mount -mtime 30


##### Exercici 7.

Trobeu tots els fitxers del vostre directori personal que tinguin extensió *java* o *sql*.

**Resposta:** 

[fanelli@Macbook DAW]$ find ~ -name "*.java" -or -name "*.sql"


##### Exercici 8.

Trobeu els fitxers ocults (regulars, o sigui no directoris) que es troben al nostre directori `$HOME`

**Resposta:**

[fanelli@Macbook DAW]$ find $HOME -type f -name ".*"


##### Exercici 9.

Trobeu els subdirectoris que pengen directament del nostre $HOME (i.e sense incloure els subdirectoris dels subdirectoris)

**Resposta:**

[fanelli@Macbook DAW]$ find $HOME -maxdepth 1 -type d


##### Exercici 10.

Executeu les següents instruccions des del vostre HOME per exemple:

```
[jamoros@heaven ~]$ ls -la
total 520592
drwx--x--x. 24 jamoros inf       4096 Nov 19 11:47 .
drwxr-xr-x. 17 root    root      4096 Oct 24 15:38 ..
-rw-------.  1 jamoros inf      17061 Nov 19 11:32 .bash_history
-rw-------.  1 jamoros inf         18 Sep  4 13:29 .bash_logout
...
```

```
[jamoros@heaven ~]$ find -maxdepth 1 -type d | wc -l
23
```

Veieu alguna relació entre el *24* de la línia del directori `.` a la primera instrucció i el *23* de la 2a instrucció?

**Resposta:** 

Se trata del número de directorios que existen dentro de mi directorio personal.

##### Exercici 11.

Comenteu quina és la diferència entre les següents ordres:
```
find -name "*.sh" -print -exec cat '{}' \;
find -name "*.sh" -print | xargs cat
```
---

**Resposta:**

No hay diferencia, hacen lo mismo. 

##### Exercici 12.

Creeu un directori anomenat 'pare' que contingui tres arxius anomenats fill1.txt, fill2.txt i fill3.txt amb el text “Hola, soc el fill NUM”, on NUM es el numero de fill de cada fitxer. Comprimiu cadascú d'aquests fitxers amb gzip i poseu-los en un directori anomenat “paregz”.

1. Coneixeu alguna família de comandes que permeti visualitzar els continguts dels fitxers comprimits sense tenir que descomprimir-los?

**Resposta:**

[fanelli@Macbook pare]$ zcat fill1.txt.gz fill2.txt.gz fill3.txt.gz 


2. Comprimiu el directori *pare* i tot el seu contingut mitjançant `gzip` amb el grau màxim de compressió. Feu el mateix utilitzant `bzip2`. Compareu els resultats i comenteu-los. Creieu que les conclusions que n'heu extret d'aquesta prova són extensibles a la compressió d'altres fitxers? per què?

**Resposta:**

[fanelli@Macbook ~]$ tar -zcvf pare.tar.gz pare

Al ser ficheros tan pequeños en tamaño gzip no puede desplegar toda su capacidad de compresión, esto sólo puede apreciarse con ficheros realmente grandes y en especial si son ficheros de texto.

3. Descomprimiu els dos últims fitxers que heu creat (*.gz* i *.bz2*) en directoris diferents, i comproveu de forma automàtica que els continguts siguin idèntics.

**Resposta:**

[fanelli@Macbook ~]$ tar dvf pare.tar.gz 

##### Exercici 13.

Contesteu les següents qüestions sobre la comanda `file` i els magic numbers:

* On es configuren i defineixen tots els magic numbers usats per la comanda `file`?

**Resposta:**

The information
     identifying these files is read from the compiled magic
     file /usr/share/misc/magic.mgc, or the files in the direc‐
     tory /usr/share/misc/magic if the compiled file does not
     exist.  In addition, if $HOME/.magic.mgc or $HOME/.magic
     exists, it will be used in preference to the system magic
     files.


* Quina funció fa la comanda `hexdump`?

**Resposta:**

  The hexdump utility is a filter which displays the specified files,  or
       standard input if no files are specified, in a user-specified format.



* Creeu un arxiu *tar* (podeu crear un conjunt d'arxius qualsevol o be copiar alguns arxius existents) i a continuació comprimiu-lo amb gzip generant per exemple l'arxiu arxiu.tar.gz. Feu la mateixa operació però comprimint amb bzip2 obtenint per exemple el fitxer arxiu2.tar.bz2. (es poden fer servir els arxius comprimits de la pregunta anterior)

**Resposta:**

[fanelli@Macbook pare]$ tar -zcvf archivo.tar.gz fill1.txt.gz fill2.txt.gz fill3.txt.gz 

[fanelli@Macbook pare]$ tar cvjf archivo.tar.bz2 fill1.txt.gz fill2.txt.gz fill3.txt.gz



* Mitjançant la comanda hexdump comproveu que cada tipus de fitxer (gzip i bzip2) es corresponen amb l'especificat a la definició del magic number corresponent al seu format (feu man hexdump per veure els diferents tipus de sortida de la comanda). Comenteu el que heu obtingut/vist.

**Resposta:**

[fanelli@Macbook pare]$ hexdump -C archivo.tar.gz

En internet he encontrado listas de los números mágicos de cada tipo de archivo y el 1F 8B es el de los archivos .gz que en este caso aparece al ejecutar hexdump

****************
---

Diferències entre mtime, ctime, atime (paràmetres de l'ordre find):

* http://www.geekride.com/inode-structure-ctime-mtime-atime/

* http://www.linuxtotal.com.mx/index.php?cont=info__tips_022


[Curiositats amb comprimits recursius](https://research.swtch.com/zip)

