# Instal·lació de Fedora 32 a portàtil - Macbook 7.1

## Especificacions

* **RAM**  8 Gb DDR2

* **Disc Dur**  Crucial SSD 240Gb + Thoshiba HDD 500Gb
*  **Targeta Gràfica** Integrated Nvidia GeForce 320M with 256 MB shared with main memory
* **Targeta Wifi** AirPort Extreme (802.11a/b/g/n)

## Tipus d'arrencada

Arrencada dual amb Fedora 32 o OSX El Capitan

## Tipus d'instal·lació: BIOS legacy o UEFI 

UEFI, en teoría

## Links de referència per fer la instal·lació

[Cómo instalar Linux en Mac](https://marcosmarti.org/como-instalar-linux-pc-mac/)
 
[Dual-Boot: Mac OS + Linux](https://www.kirsle.net/wiki/Fedora-on-Macbook)

## Links de referència de problemes d'algun element del maquinari del portàtil amb Fedora 32

[rEFIt](https://www.rodsbooks.com/refind/)  Un boot manager que es necesari instalar per tal de tenir dual boot en el Mac

## Canvis a la BIOS en funció de la instal·lació escollida

Instal·lació de rEFIt, però no va a funcionar mai

## Tipus de taula de particions escollida 

GUID Partition Table (GPT)

## Preparació particions

En el meu cas vaig contaba amb un disc dur SSD de 240Gb. Per altra banda el meu reproductor de CD Superdrive va deixar de funcionar amb alguna de les actualizacions del SO, per el que vaig sustituir el Superdrive per un altre disc dur HDD. Així, el resultat va a ser un macbook amb **el SO i programes en un rápid disc SSD amb les particions i per altra banda els arxius al disc HDD**, mes fiable a la hora de sobreescriure moltes vegades.
De aquesta manera vaig fer una partició de 165 i altra de 75Gb para cadescú dels SOs. La destinada a Fedora la vaig fer en format ExFAT, suposadament un format aceptat tant per Mac com per Linux. La realitat es que va fallar.

## Instal·lació

Va constar de diferents pasos:

* El primer paso va ser pedir ajuda al profesor, el cual me va dir que el que volía fer era ilegal i que jo era l'unic alumne de la classe a qui no podía ajudar. Això em va deixar amb les opcions de; **o be comprar un nou ordinador per un mòdic preo amb uns diners que no tenía, o be cercar-me la vida**. Vaig escollir l'unica opció que podía. 

* Llavors vaig descarregar la imatge del SO per tal de bootear Fedora en mi macbook, per això calía **instal·lar la imatge en un USB booteable** i es el que vaig fer. El fer-ho em va suposar moltes dificultats ja que van fallar diferents programes destinats a crear aquest tipo de imatges USB.

* Desprès vaig instal·lar el software rEFIt tal i com deian a les numeroses webs que vaig consultar, inclosa la pàgina del propi software. 

* Per últim vaig instal·lar el SO Fedora desde el USB que había preparat. Tot va anar be fins el moment de triar la partició on fer l'instalació. Va ser imposible fer-ho al lloc on volía, crec que potser es devia al format, aixì que vaig acabar instalant-ho amb l'opció que feia que Fedora crease la partició que necessitava, o sigui _Configuración de almacenamiento automática_. 

![instalacion](https://blog.desdelinux.net/wp-content/uploads/2018/05/Instalacion-de-Fedora-28-4.jpg)

* Quan vaig reiniciar el macbook em vaig adonar que el software rEFIt no funcionava i **no em donaba opció a triar el SO amb el que volia arrancar el ordinador**. Pero en qualsevol cas Fedora habia sigut instal·lat en una partició del macbook.

* Al final vaig intentar arrancar l'ordinador amb la tecla Alt pulsada, cosa que va a funcionar, mostrando una llista dels discs d'arranque disponibles: mac OS i Fedora.

## Post-instal·lació

Després de l'instal·lació vaig seguir les indicacions del repositori [fed-at-inf](https://gitlab.com/jordinas/fed-at-inf) i també vaig entrar de ple al mòn dels _tweaks_ amb webs com [aquesta](https://www.dedoimedo.com/computers/fedora-32-essential-tweaks.html).

![fedoratweaks](https://www.dedoimedo.com/images/computers-years/2020-1/fedora-32-tweaks-extensions-enable.png)

## Problemes i resolucions

Un problema que vaig tenir va ser que el wifi no va funcionar amb el nou sistema operatiu. Per solucionar-ho vaig cercar informació per tota la xarxa fins encontrar [aquest enllaç](https://www.kirsle.net/wiki/Fedora-on-Macbook) on diu quines ordres feian falta per reparar el problema. Lo mateix em va a passar amb el format exFAT, vaig tenir que descarregar uns paquets per fer Fedora capaç de llegir aquet format.




