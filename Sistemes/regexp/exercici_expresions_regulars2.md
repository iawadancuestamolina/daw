### Exercicis d'expressions regulars amb sed i grep

##### Exercici 1

Del resultat de fer un head de les 15 primeres línies del fitxer `/etc/passwd`
mostrar les línies que contenen un 2 en algun lloc.
````
head -15 | grep 2
````

##### Exercici 2

Del resultat de fer un head de les 15 primeres línies del fitxer `/etc/passwd`
mostrar només les línies que tenen *uid* 2.
```
head -15 | grep .*:.*:2:
```

##### Exercici 3

Usant grep valida si un dni té el format apropiat.

```
echo "12345678A" | egrep ^[[:digit:]]{8}[A-Z]$
```

##### Exercici 4 

Usant grep valida si una data té el format dd-mm-aaaa.
```
 echo -e "04-09-1990" | egrep "^[0-9]{2}-[0-9]{2}-[0-9]{4}$"
```

##### Exercici 5

Usant grep valida si una data té el format dd/mm/aaaa.
```
 echo -e "04-09-1990" | egrep "^[0-9]{2}/[0-9]{2}/[0-9]{4}$"
```

##### Exercici 6 

Usant grep validar si una data té un format vàlid. Els formats poden ser: dd-mm-aaaa o dd/mm/aaaa.
```
 echo -e "04/09/2009\n04-09-1990" | egrep "(^[0-9]{2}-[0-9]{2}-[0-9]{4}$)|(^[0-9]{2}/[0-9]{2}/[0-9]{4}$)"
```

##### Exercici 7 

Buscar totes les línies del fitxer noms1.txt que tenen la cadena *Anna* o la cadena *Jordi*
```
egrep "Anna|Jordi" nom.txt
```

##### Exercici 8

Substituir del fitxer noms1.txt tots els noms Anna i Jordi per -nou-.

##### Exercici 9

Del resultat de fer un `head` (10 línies) del fitxer `/etc/passwd` substituir `/sbin/nologin` per `-noshell`.

##### Exercici 10

Ídem que l'exercici anterior però fent la substitució només de les línies 4 a la 8.

##### Exercici 11

Ídem exercici anterior però fent les substitucions des de la línia que conté *adm* fins la línia que conté *halt*.

