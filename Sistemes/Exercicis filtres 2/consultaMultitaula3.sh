#Capçalera

# Script que intenta imitar l'anterior consultaMultitaula.sh, amb un format diferent, però que ha perdut una mica de codi.
# Entén el que hi ha i  
dona informacio sobre un treballador de repventas respecte a la seva oficina
# Es necessita que tenir els fitxers repventas.dat i oficinas.dat al mateix directori on s'executa l'script


# Emmagatzemmem els fitxers originals 

taula_repventas=repventas.dat 
taula_oficinas=oficinas.dat

# Establim el delimitador
delimitador=$'\t'



echo "escriu el nombre del treballador de l'oficina"
read numVenedor
echo "escriu el nombre de l'opcio que vols"
echo "1: nombre de l'oficina"
echo "2: ciutat de l'oficina"
echo "3: nombre del director de l'oficina"
echo "4: nombre de l'oficina"
echo "5: objectiu de l'oficina"
echo "6: vendes de l'oficina"
read campOficina

case "$campOficina" in
	1)
		cadenaOpcio="el nombre de l'oficina"
		;;
	2)
		cadenaOpcio="la ciutat de la oficina"
		;;
	3)
		cadenaOpcio="la regió de l'oficina"
		;;
	4)
		cadenaOpcio="el nombre del director de l'oficina"
		;;
	5)
		cadenaOpcio="l'objectiu de l'oficina"
		;;
	6)
		cadenaOpcio="les vendes de l'oficina"
		;;
	*)
	echo "escull una opcio vàlida [1-6]"
	exit 1
		;;
esac

# Creem els fitxers ordenats, repventas i oficinas pel 1er     camp
cat $taula_repventas | sort -t "$delimitador" -k1 > ${taula_repventas}ord1
cat $taula_oficinas | sort -t "$delimitador" -k1  > ${taula_oficinas}ord1


# Mostrem el camp escollit
echo $numOficina | join ${taula_oficinas}ord1 -  -t"$delim    rtador"  -1 1 -2 1  | cut -f$campOficina



if [numOficina=$(echo "$numVenedor" | join ${taula_repventas}ord1 - -t"$delimitador" -1 1 -2 1 | cut -f4 -d"$delimitador")]
then
		echo "$cadenaOpcio del venedor $numVenedor és $(join ${taula_oficinas}ord1 - -t"$delimitador" -1 1 -2 1 | cut -f$campOficina)"
else
	echo "o $numVenedor no existeix a la taula o no té oficina"
fi

