/*
 * Program.java    1.0 04/01/2021
 *
 * L'objectiu d'aquest mètode és escriure per pantalla un histograma amb els pesos d'un conjunt de persones.
 *
 * Copyright 2020 Keila Gonzalez <1hiawgonzalezkeila@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class Program {
  
  /**
   * L'objectiu d'aquest mètode és escriure per pantalla un histograma amb els pesos d'un conjunt de persones.
   * 
   * @param a, array de reals.
   */
  public void histogram(double a[]){
      int aux = 0;
      for(int k = 0; k <= 100; k++){
          for(int i = 0; i < a.length; i++){
              if (a[i] == k){
                  aux++;
              }
          }  
          if (aux > 0){
              System.out.printf("%d: ", k);
              for(int x = 0; x < aux; x++){
                  System.out.printf("*");
              }
              System.out.println();
              aux = 0;
          }

      }
  }
}