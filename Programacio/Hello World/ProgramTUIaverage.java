/*
 *  Program. java         1.0 5/11/2020
 * 
 * Copyright 2020 Adán Cuesta Molina <1hiawcuestamolinaadan@gmail.com>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information
 */

import java.util.Scanner;
 
public class ProgramTUIaverage {
                        
   /**
   * Calculate the average of two numbers.
   * 
   * 
   * @param n1 a real number
   * @param n2 a real number.

   * @return the average of -n1- and -n2-.
   */
    
    public int average2(int n1, int n2) {
        return (n1 + n2) / 2;
    }
    
    
    /*
     * 
     * @param args not used
     */
    
    public static void main(String[] args) {
        //definimos las variables para (x1,y1) (x2,y2) (mx,my)
        int x1, x2, y1, y2, mx, my;
                        Scanner s = new Scanner(System.in);
                        //preguntar el punto A
                        System.out.println("Introuce las coordenadas de un punto A");
                        System.out.print("X: ");
                        x1 = s.nextInt();
                        System.out.print("Y: ");
                        y1 = s.nextInt();
                        //preguntar el punto B
                        System.out.println("Introuce las coordenadas de un punto B");
                        System.out.print("X: ");
                        x2 = s.nextInt();
                        System.out.print("Y: ");
                        y2 = s.nextInt();
                        //hacer la media
                  ProgramTUIaverage p = new ProgramTUIaverage();
                  mx = p.average2(x1, x2);
                  my = p.average2(y1, y1);
                  
                        
                        System.out.print("La media del punto A (" + x1 + "," + y1 + ") y el punto B (" + x2 + "," + y2 + ") es el punto m (" + mx + "," + my + ").");
    
    

    
    }
}
        