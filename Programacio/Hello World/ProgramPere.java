/* 2/11/20
 * 
 * 
 * */

import java.util.Scanner;

public class ProgramPere {
    
    /** 
     * ask the name, age and weight and display it
     * 
     * @param args Not used.
     */
    
    public static void main(String[] args) {
        String nom;
        Double pes;
        int edat;
                Scanner s = new Scanner(System.in);
        //preguntamos y guardamos el nombre
        System.out.print("Com et dius?");
        nom = s.nextLine();
        //saludamos y ponemos el nombre
        System.out.println("Hola " + nom + "!!!");
        System.out.print("Quina edat tens?");
        //preguntamos y guardamos la edad y el peso
        edat = s.nextInt();
        System.out.print("Quan peses?");
        pes = s.nextDouble();
        //Escribimos el peso y la edad y el nombre
        System.out.print("En "+ nom + " té " + edat + " anys i pesa " + pes + " kilos.");
    }
}