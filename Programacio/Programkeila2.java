/*
 * Program.java    1.0 04/01/2021
 *
 * Donada una taula de nombres enters ordenats de forma creixent i un nombre enter, 
 * determina si el nombre és un element de la taula emprant cerca dicotòmica. 
 *
 * Copyright 2020 Keila Gonzalez <1hiawgonzalezkeila@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.Arrays;
public class Program {

    /**
     * Donada una taula de nombres enters ordenats de forma creixent i un nombre enter, 
     * determina si el nombre és un element de la taula emprant cerca dicotòmica. 
     * 
     * @param a, array de paraules.
     */
    public boolean binarySearch(int a[], int n){
        int center, low, top;
        boolean found = false;
        Arrays.sort(a);
        low = 0;
        top = a.length -1;
        while(low <= top && !found){
            center = (top + low) / 2;
            if (a[center] == n){
                found = true;
            } else if (n < a[center]){
                top = center -1;
            } else {
                low = center + 1;
            }
        }
        return found;
    }
}