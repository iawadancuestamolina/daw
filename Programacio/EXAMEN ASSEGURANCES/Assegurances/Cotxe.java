/*
 * Cotxe.java
 * 		
 * Copyright 2011 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */

 

public abstract class Cotxe {
	/** La matrícula del cotxe. */
	private String matricula;
	/** La marca del cotxe. */
	private String marca;
	/** El model del cotxe. */
	private String model;

	/**
	 * Constructor.
	 * 
	 * @param matricula La matrícula del cotxe.
	 * @param marca La marca del cotxe.
	 * @param model El model del cotxe.
	 */
	public Cotxe(String matricula, String marca, String model) {
		this.matricula = matricula;
		this.marca = marca;
		this.model = model;
	}

	// Getters i setters.
	public String getMatricula() {
		return matricula;
	}

	public String getMarca() {
		return marca;
	}

	public String getModel() {
		return model;
	}

}
