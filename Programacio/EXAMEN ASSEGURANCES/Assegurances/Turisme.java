/*
 * Turisme.java
 *   
 * Copyright 2011 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */

public class Turisme extends Cotxe {

    public Turisme(String matricula, String marca, String model) {
        super(matricula, marca, model);
    }
}