/*
 * Main.java
 *   
 * Copyright 2011 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */

 

public class Main {

 /**
  * @param args
  */
 public static void main(String[] args) {
  EmpresaAssegurances ea = new EmpresaAssegurances();
  Polissa p1 = ea.afegeixPolissa("65664777R", "Anna Grau   ", "10/12/1980", "1235ABC", "Seat",
   "León", "turisme", "tercers", false);
  p1.setDataContractacio("01/02/2009");
  Polissa p2 = ea.afegeixPolissa("12345678L", "Joan Pino    ", "11/02/1965", "4566XYZ", "Seat",
   "Ibiza", "turisme", "totrisc", false);
  p2.setDataContractacio("01/01/2008");
  Polissa p3 = ea.afegeixPolissa("45684288R", "Maria Garcia", "11/05/1972", "4568FFG",
   "Reanult", "Clio", "turisme", "tercers", false);
  p3.setDataContractacio("01/05/2009");
  Polissa p4 = ea.afegeixPolissa("45678925R", "Jaume Font  ", "22/02/1983", "4448EEE", "Ford",
   "Fiesta", "turisme", "totrisc", true);  
  ea.afegeixPolissa("65664777R", "Marga Punti", "19/03/1952", "6535FGT", "Renault", "Scenic",
   "monovolum", "totrisc", false);
  ea.afegeixPolissa("65664777R", "Moisés Artia", "19/03/1955", "4644FGT", "Renault", "Scenic",
   "monovolum", "totrisc", true);
  ea.afegeixPolissa("65664777R", "Joana Roca  ", "12/04/1987", "4584TRD", "VW", "Sharan",
   "monovolum", "tercers", false);
  ea.afegeixPolissa("45678925R", "Lluís Arbre", "12/04/1973", "5811TRD", "VW", "Sharan",
   "monovolum", "tercers", false);
  ea.afegeixPolissa("36735252H", "Glòria Gómez", "12/04/1990", "3677GGG", "VW", "Sharan",
   "monovolum", "totrisc", true);

  p1.afegeixAccident("10/02/2011", "Barcelona").setCost(300);
  p1.afegeixAccident("10/05/2011", "Barcelona").setCost(3000);
  p1.afegeixAccident("10/03/2011", "Barcelona").setCost(200);
  p1.afegeixAccident("10/02/2011", "Barcelona").setCost(100);
  p1.afegeixAccident("10/01/2011", "Barcelona").setCost(100);

  p2.afegeixAccident("10/02/2011", "Barcelona").setCost(50);

  p3.afegeixAccident("10/02/2011", "Barcelona").setCost(350);
  p3.afegeixAccident("10/05/2011", "Barcelona");
  p3.afegeixAccident("10/03/2011", "Barcelona");
  p3.afegeixAccident("10/02/2011", "Barcelona");
  p3.afegeixAccident("10/01/2011", "Barcelona");

  p4.afegeixAccident("10/02/2011", "Barcelona").setCost(150);
  p4.afegeixAccident("10/05/2011", "Barcelona").setCost(200);
  p4.afegeixAccident("10/03/2011", "Barcelona").setCost(300);
  p4.afegeixAccident("10/02/2011", "Barcelona").setCost(400);
  p4.afegeixAccident("10/01/2011", "Barcelona").setCost(500);

  ea.llistaPolissesNoVencudes();
  ea.llistaAccidents();
  System.out.println("S'han renovat " + ea.renovaPolisses() + " polisses");
  ea.llistaPolissesNoVencudes();
  ea.llistaAccidents();

 }

}