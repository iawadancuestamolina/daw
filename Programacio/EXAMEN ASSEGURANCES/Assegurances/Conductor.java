/*
 * Conductor.java
 *   
 * Copyright 2011 NOM I COGNOMS <MAIL>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */



 import java.time.LocalDateTime;
 import java.time.temporal.*;

public class Conductor {
 /** El DNI del conductor */
 private String dni;
 /** El nom del conductor. */
 private String nom;
 /** La data de naixement del conductor */
 private String dataNaixement;

 /**
  * Constructor.
  * 
  * @param dni El DNI del conductor
  * @param nom El nom del conductor.
  * @param dataNaixement La data de naixement del conductor.
  */
 public Conductor(String dni, String nom, String dataNaixement) {
  this.dni = dni;
  this.nom = nom;
  this.dataNaixement = dataNaixement;
 }

 /**
  * Calcula quants anys té el conductor en la data passada per paràmetre. Si la data és anterior
  * a
  * la data de naixement retorna 0.
  * 
  * @param data Data en format DD/MM/AAAA.
  * @return L'edat del conductor en anys o 0.
  */
 public int edat(String data) {
     LocalDateTime hoy = LocalDateTime.now();
     LocalDateTime birth = JodaDT.parseDDMMYYYY(data);//convertimos la fecha de nacimiento en objeto para operar
     long edad = birth.until(hoy, ChronoUnit.YEARS);
     return (int)edad;
 }

 // Getters i setters.

 public String getDni() {
  return dni;
 }

 public String getNom() {
  return nom;
 }

}
