/*
 * EmpresaAssegurances.java
 *   
 * Copyright 2011 NOM I COGNOMS <MAIL>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */

 

import java.util.ArrayList;


import java.time.LocalDateTime;

public class EmpresaAssegurances {

 /** Conjunt de pòlisses de l'empresa */
 private ArrayList<Polissa> polisses;

 /**
  * Constructor per defecte.
  */
 public EmpresaAssegurances() {
  polisses = new ArrayList<Polissa>();
 }

 /**
  * Afegeix una pòlissa a l'empresa.
  * 
  * @param p La pòlissa a afegir.
  * @return La polissa aefgida si s'ha pogut afegir, null altrament.
  */
 public Polissa afegeixPolissa(Polissa p) {
  boolean afegit = polisses.add(p);
  if (afegit) {
   return p;
  }
  return null;
 }

 /**
  * A partir de les dades passades per paràmetre, s'afegeix una pòlissa a l'empresa a data
  * actual.
  * 
  * La pòlissa que afegim ha de tenir el conductor, el cotxe i el preu adients segons les dades
  * que passem per paràmetre..
  * 
  * @param dni El dni del conductor.
  * @param nom El nom del conductor.
  * @param dataNaixement La data de naixement del conductor.
  * @param matricula La matrícula del cotxe.
  * @param marca La marca del cotxe.
  * @param model El model del cotxe.
  * @param tipusCotxe El tipus de cotxe: "turisme" o "monovolum"
  * @param tipusPolissa El tipus de pòlissa que es vol contractar: "tercers" o "totrisc"
  * @param coberturaEstranger Si es desitja o no tenir cobertura a l'estranger. Si l'assegurança
  *        és "tercers" aquest paràmtre serà sempre false.
  * @return La pòlissa afegida si s'ha pogut afegir, null altrament.
  */
 public Polissa afegeixPolissa(String dni, String nom, String dataNaixement, String matricula,
  String marca, String model, String tipusCotxe, String tipusPolissa,
  boolean coberturaEstranger) {
     //creamos una nueva póliza y la añadimos al final
     Polissa x;
     if (tipusPolissa == "tercers") {
              x = new Tercers(JodaDT.formatDDMMYYYY(LocalDateTime.now()));
     } else {
              x = new TotRisc(JodaDT.formatDDMMYYYY(LocalDateTime.now()), coberturaEstranger);
     }

     
//creamos un conductor y lo añadimos
Conductor pepe = new Conductor(dni,nom,dataNaixement);
     x.setConductor(pepe);
     
     //creamos un nuevo coche y lo añadimos
     if (tipusCotxe == "monovolum") {
         Monovolum coche = new Monovolum(matricula,marca,model);
         x.setCotxe(coche);
     } else {
        Turisme coche = new Turisme(matricula,marca,model);
         x.setCotxe(coche); 
     }
     
     //añadimos el precio de la póliza
     x.posaPreuPolissaNova();
     
     //añadimos la póliza al array
     polisses.add(x);
     
     
  return x;
 }

 /**
  * Recorre totes les pòlisses de l'empresa i renova aquelles que es poden renovar.
  * 
  * @return El nombre de pòlisses que s'han pogut renovar.
  */
 public int renovaPolisses() {
     int contador = 0;
     for (Polissa x : polisses) {
         if (x.vencuda()) {
             x.renova();
             contador++;
         } 
     }
  return contador;
 }

 /**
  * Llista totes aquelles pòlisses que no han estat vençudes.
  * Disposeu del format desitjat a l'enunciat de l'examen.
  */
 public void llistaPolissesNoVencudes() {
     System.out.printf("%-10s\t%-13s\t%-10s\t%-10s\t%-10s\t%-10s\t%-10s\t%-10s\n","DNI","NOM","MATRICULA","MARCA","MODEL","PREU","DATA_VENC","ESTRANGER");
     for (Polissa x : polisses) {
      if (!x.vencuda()) {
          System.out.println(x.toString());
      }
     }
     System.out.println();
 }

 /**
  * Llista tots els accidents corresponent a pòlisses no vençudes i mostra el total pagat per
  * l'empresa.
  * Disposeu del format desitjat a l'enunciat de l'examen.
  */
 public void llistaAccidents() {
     System.out.printf("%-10s\t%-10s\t%-10s\t%-10s\n","ID","DATA","LLOC","PREU");
     double coste = 0;

     for ( Polissa x : polisses) {
           ArrayList<Accident> accidentes = x.getAccidents();
         for (Accident y : accidentes) {
             System.out.println(y.toString());
             coste += y.getCost();
             
         }
     }
     System.out.println("Coste total accidentes: " + coste + "\n");
 }
}
