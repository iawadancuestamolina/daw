/*
 * TotRisc.java
 *   
 * Copyright 2011 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */

public class TotRisc extends Polissa {
    
    //ATTRIBUTES
    private static final double QUOTA_TURISME = 1000;
    private static final double QUOTA_MONOVOLUM = 3000;
    private boolean coberturaEstranger;
    private static final double QUOTA_ESTRANGER = 20;
    //METHODS
    public TotRisc(String dataContractacio, boolean coberturaEstranger) {
        super(dataContractacio);
        this.coberturaEstranger = coberturaEstranger;
    }
    
    public double preuBaseSegonsPolissa() {
        if (this.cotxe instanceof Turisme) {
            return this.QUOTA_TURISME + 100 * this.QUOTA_ESTRANGER;
        } else {
            return this.QUOTA_MONOVOLUM + 200 * this.QUOTA_ESTRANGER;
        }
    }
    
    @Override
    public String toString() {
        Conductor x = this.conductor;
        Cotxe y = this.cotxe;
        
        String mensaje = x.getDni() + "\t" + x.getNom() + "\t" + y.getMatricula() + "\t" + y.getMarca() + "\t" + y.getModel() + "\t" + 
            this.preu + "\t" + this.dataVenciment();
        mensaje += ((this.coberturaEstranger == true) ? "   *" : "");
            return mensaje;
    }
}