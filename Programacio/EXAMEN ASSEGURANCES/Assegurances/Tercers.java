/*
 * Tercers.java
 *   
 * Copyright 2011 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */

public class Tercers extends Polissa {
    
    //ATTRIBUTES
    private static final double QUOTA_TURISME = 400;
    private static final double QUOTA_MONOVOLUM = 800;
    //METHODS
    public Tercers(String dataContractacio) {
        super(dataContractacio);
    }
    
    public double preuBaseSegonsPolissa() {
        if (this.cotxe instanceof Turisme) {
            return this.QUOTA_TURISME;
        } else {
            return this.QUOTA_MONOVOLUM;
        }
    }
    
    @Override
    public String toString() {
       Conductor x = this.conductor;
        Cotxe y = this.cotxe;
        
        String mensaje = x.getDni() + "\t" + x.getNom() + "\t" + y.getMatricula() + "\t" + y.getMarca() + "\t" + y.getModel() + "\t" + 
            this.preu + "\t" + this.dataVenciment();

        return mensaje;
    }
}