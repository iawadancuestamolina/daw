/*
 * Polissa.java
 *   
 * Copyright 2011 NOM I COGNOMS <MAIL>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */

 

import java.util.ArrayList;


import java.time.LocalDateTime;

public abstract class Polissa {
 /** Data de conrtactació de l'assegurança */
 protected String dataContractacio;
 /** Preu de l'assegurança */
 protected double preu;
 /** Cotxe que protegeix l'assegurança */
 protected Cotxe cotxe;
 /** Conductor que contracta l'assegurança */
 protected Conductor conductor;
 /** Accidents tramesos a l'assegurança */
 private ArrayList<Accident> accidents;

 /** Edat fins a la qual s'aplica una penalització per ser jove */
 private static final int EDAT_LIMIT = 25;
 /** Precetatge de penalització per ser jove */
 private static final double PERCENTATGE_PENALITZACIO_EDAT = 10;
 /** Percentatge que no podem sobrepassar amb costos d'accident per no ser penalitzats */
 private static final double PERCENTATGE_LIMIT_ACCIDENTS = 10;
 /** Percentatge de descompte o augment del preu de la pòlissa */
 private static final double PERCENTATGE_DESCOMPTE_AUGMENT = 7;

 /**
  * Constructor.
  * 
  * @param dataContractacio La data de contractació de la pòlissa.
  */
 public Polissa(String dataContractacio) {
  this.dataContractacio = dataContractacio;
  this.accidents = new ArrayList<Accident>();
 }

 /**
  * Calcula el preu base segons el tipus de pòlissa contractada. Per saber com fer el càlcul
  * consulteu la taula adjunta al'enunciat.
  * 
  * @return El preu base.
  */
 public abstract double preuBaseSegonsPolissa();

 /**
  * Posa el preu a la pòlissa a partir del preu base i aplicant
  * l'augment corresponent si el conductor és menor de EDAT_LIMIT anys.
  * 
  * Aquest mètode servirà per posar el preu a la pòlissa quan aquesta es dóna d'alta.
  */
 public void posaPreuPolissaNova() {
     if (this.conductor.edat(dataContractacio) > EDAT_LIMIT) {
         this.preu = this.preuBaseSegonsPolissa() * (1 + this.PERCENTATGE_PENALITZACIO_EDAT / 100);
     } else {
         this.preu = this.preuBaseSegonsPolissa();
     }
 }

 /**
  * Calcula la data de venciment de la pòlissa. La pòlissa venç després d'un any d'haver estat
  * creada.
  * 
  * @return La data de venciment en format DD/MM/AAAA
  */
 public String dataVenciment() {
     LocalDateTime data = JodaDT.parseDDMMYYYY(this.dataContractacio);
     LocalDateTime dataVenciment = data.plusYears(1);
  return JodaDT.formatDDMMYYYY(dataVenciment);
 }

 /**
  * Calcula si la pòlissa està vençuda a la data actual.
  * 
  * @return true si la pòlissa està vençuda, false altrament.
  */
 public boolean vencuda() {
     LocalDateTime hoy = LocalDateTime.now();
     LocalDateTime fechaVenc = JodaDT.parseDDMMYYYY(this.dataVenciment());
     LocalDateTime creacion = JodaDT.parseDDMMYYYY(this.dataContractacio);
     if (JodaDT.isInInterval(fechaVenc,creacion,hoy)) {
         return true;
     } else {
         return false;
     }
 }

 /**
  * Renova la pòlissa. Només es renovarà la pòlissa si aquesta ha estat vençuda.
  * En la renovació hem de:
  * - Calcular el nou preu a partir del càlcul de preu de renovació.
  * - Canviar la data de contractació i posar-la a dia d'avui.
  * - Esborrar tots els accidents associats a la pòlissa.
  * 
  * @return true si s'ha renovat, false altrament.
  */
 public boolean renova() {
     this.dataContractacio = JodaDT.formatDDMMYYYY(LocalDateTime.now());
     accidents.clear();
     this.preu = this.preuRenovacio();
  return true;
 }

 /**
  * Calcula el preu de renovació:
  * 
  * -Si el valor de les reparacions dels comunicats d'accident en l'últim període és menor del
  * PERCENTATGE_LIMIT_ACCIDENTS% del valor de la seva pòlissa en aquest període, es farà un
  * descompte del preu global del PERCENTATGE_DESCOMPTE_AUGMENT%.
  * 
  * - Si el valor de les reparacions dels parts en l'últim període és major o igual del
  * PERCENTATGE_LIMIT_ACCIDENTS% del valor de la seva pòlissa en aquest període però menor al
  * valor total de la pòlissa, no s'aplica cap descompte ni augment.
  * 
  * - Altrament s'incrementa el preu en un PERCENTATGE_DESCOMPTE_AUGMENT%.
  * 
  * NOTA: No es tindrà en compte si el conductor ha sobrepassat l'edat EDAT_LIMIT.
  * 
  * @return El preu de renovació.
  */
 public double preuRenovacio() {
     double valorReparacions = 0;
     double nuevoPreu;
     //recorremos el array de accidentes sumando las reparaciones
     for (Accident x : accidents) {
         valorReparacions += x.getCost();
     }
     
     //vemos si se aplica el descuento o no
     if (valorReparacions < this.preu * (this.PERCENTATGE_LIMIT_ACCIDENTS / 100)) {
         nuevoPreu = this.preu * (1 - this.PERCENTATGE_DESCOMPTE_AUGMENT / 100);
     } else if (valorReparacions > this.preu * (this.PERCENTATGE_LIMIT_ACCIDENTS / 100) && valorReparacions < this.preu){
         nuevoPreu = this.preu;
     } else {
         nuevoPreu = this.preu * (this.PERCENTATGE_DESCOMPTE_AUGMENT / 100 + 1);
     }
     return nuevoPreu;
 }

 /**
  * Afegeix un accident a la pòlissa.
  * 
  * @param a L'accident a afegir.
  * @return L'accident afegit si s'ha pogut afegir, null altrament.
  */
 public Accident afegeixAccident(Accident a) {
  boolean afegit = accidents.add(a);
  if (afegit) {
   return a;
  }
  return null;
 }

 /**
  * Afegeix un accident a la pòlissa a partir dels paràmtres introduïts.
  * 
  * @param data La data de l'accident en format DD/MM/AAAA.
  * @param lloc El lloc de l'accident.
  * @return L'accident afegit si s'ha pogut afegir, null altrament.
  */
 public Accident afegeixAccident(String dataHora, String lloc) {
     Accident a = new Accident(dataHora, lloc);
     accidents.add(a);
  return a;
 }

 // Getters i setters.

 public void setDataContractacio(String dataContractacio) {
  this.dataContractacio = dataContractacio;
 }

 public void setCotxe(Cotxe cotxe) {
  this.cotxe = cotxe;
 }

 public void setConductor(Conductor conductor) {
  this.conductor = conductor;
 }

 public ArrayList<Accident> getAccidents() {
  return accidents;
 }

}
