/*
 * Program.java        1.0 23/01/2012
 *
 * Models the program.
 *
 * Copyright 2012 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program2 {

    /**
     * Determines if an EAN code is valid.
     * 
     * @param ean an EAN code
     * @return true if it is valid, false otherwise
     */
    public boolean isValidEAN(String ean) {
        String rev = "";
        char c;
        int senars = 0, parells = 0, n, sum;
        int ultim = Integer.parseInt(ean.substring(ean.length() - 1, ean.length()));
        
        //girar el codi menys lultim numero
        for (int i = ean.length() - 2; i >= 0; i--){
            c = ean.charAt(i);
            rev = rev + c;
        }
        
        //sumar els digits
        for (int i = 0; i < rev.length(); i++){
            n = Integer.parseInt(ean.substring(i, i + 1));
            if (i % 2 != 0){
                senars += n;
            }else{
                parells += n;
            }
        }
        
        //compr si el codi de control es el correcte
        sum = senars * 3 + parells;
        return (10 - sum % 10) % 10 == ultim;
    }
}