/*
 * Program.java        1.0 23/01/2012
 *
 * Models the program.
 *
 * Copyright 2012 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Splits a string around matches of the given substring.
     * 
     * @param s a string
     * @param sub a substring (delimiter)
     * @return an array of string with the resulting substrings
     */
    public String[] mySplit(String s, String sub) {
        //inicializar variabels
        int longitud = s.length();
        int sublong = sub.length();
        int longArr = 0;
        int contador = 0;
        int ocurrencias = 0;
        int index = 0;
        int anterior = 0;
        String sobras = "";
        
        //recorrer string buscando el patron y contando
        for (int i = 0; i < longitud; i++) {//recorre string
            if (s.charAt(i) == sub.charAt(0)) {//comprueba si puede comenzar el patron
                for (int j = 0; j < sublong; j++) {//recorre el patron
                    if (s.charAt(i+j) == sub.charAt(j)) {//comprueba si coinciden patron y string
                        contador++;
                    }
                   
                }
                
            }
            //si contador acierta con todas las letras del patrón entonces tenemos una ocurrencia en i
            if (contador == sublong) {
                ocurrencias++;
                contador = 0;
                sobras = s.substring(i,longitud);
                
            }
        }
        //mirar si hay más string después de la última ocurrencia
        if (sobras != "") {
            ocurrencias++;
        }
        String [] resultado = new String [ocurrencias];
       
        
         //VOLVEMOS A recorrer string buscando el patron y contando ESTA VEZ VAMOS A MONTAR EL ARRAY
        for (int i = 0; i < longitud; i++) {//recorre string
            if (s.charAt(i) == sub.charAt(0)) {//comprueba si puede comenzar el patron
                for (int j = 0; j < sublong; j++) {//recorre el patron
                    if (s.charAt(i+j) == sub.charAt(j)) {//comprueba si coinciden patron y string
                        contador++;
                    }
                    
                }
                
            }
            //si contador acierta con todas las letras del patrón entonces tenemos una ocurrencia en i
            if (contador == sublong) {
                contador = 0;
                resultado[index] = s.substring(anterior,i);
                index++;
                anterior = i + sublong;
            }
        }
        if (sobras != "") {
            resultado[ocurrencias-1] = sobras;
        }
     return resultado;   
    }

}
