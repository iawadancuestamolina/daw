/*
 * Program.java        1.0 24/01/2013
 *
 * Models the program.
 *
 * Copyright 2013 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program1 {
    
    /**
     * Gets the morse code for a key word.
     * 
     * @param keyWord a key word
     * @return a string with the following format "letter = morse code"
     */
    public String mnemonicMorse(String keyWord) {
        String vocals = "AEIU";
        int nVocals = 0;
        String s = keyWord.toUpperCase();
        String morse = s.charAt(0) + " = ";
        char c;
        
        //buscar el numero de vocals
        for (int i = 0; i < s.length(); i++){
            c = s.charAt(i);
            if (c == 'O'){
                morse = morse + "-";
            }else if (vocals.indexOf(c) != -1) {
                morse = morse + "·";
            }
        }
        
        return morse;
    }
}
