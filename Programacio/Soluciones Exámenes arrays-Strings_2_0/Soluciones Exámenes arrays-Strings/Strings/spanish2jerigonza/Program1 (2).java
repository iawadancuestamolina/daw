/*
 * Program.java        1.0 23/01/2012
 *
 * Models the program.
 *
 * Copyright 2012 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program1 {

    /**
     * Translates a word from spanish to jerigonza.
     * 
     * @param s a word
     * @return the translated word
     */
    public String spanish2jerigonza(String s) {
        String vocals = "aeiou";
        String var = "";
        s = s.toLowerCase();
        char c;
        
        for (int i = 0; i < s.length(); i++){
            c = s.charAt(i);
            if(vocals.indexOf(c) != -1){
                var = var + c + 'p' + c;
            }else{
                var = var + c;
            }
        }
        return var;
    }

    /**
     * Translates a word from english to basic pig latin.
     * 
     * @param s a word
     * @return the translated word
     */
    public String english2basicPigLatin(String s) {
        String vocals = "aeiou";
        String var = "";
        s = s.toLowerCase();
        char c;
        
        if(vocals.indexOf(s.charAt(0)) != -1){
            var = s + "ay";
        } else {
            for (int i = 0; i < s.length(); i++){
                c = s.charAt(i);
                if(vocals.indexOf(c) != -1){
                    var = s.substring(i,s.length()) + s.substring(0,i) + "ay";
                }
            }
        }
        return var;
    }
}