/*
 * Program.java        1.0 24/01/2013
 *
 * Models the program.
 *
 * Copyright 2013 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Random;
import java.util.Scanner;

public class Program2 {

    /**
     * Builds a string with as many "-" as a secretWord length.
     * 
     * @param secretWord a word
     * @return the hidden word
     */
    public String hide(String secretWord) {
        String s = "";
        for (int i = 0; i < secretWord.length(); i++){
            s = s + "-";
        }
        return s;
    }

    /**
     * Builds a string with the hiddenWord refresehed if the letter is in secret word.
     * 
     * @param secretWord a secret word
     * @param hiddenWord teh corresponding hiddwen word
     * @param letter a letter
     * @return a new refreshed hidden word
     */
    public String refresh(String secretWord, String hiddenWord, String letter) {
        char c = letter.charAt(0);
        char c2;
        char[] tal = hiddenWord.toCharArray();
        String algo = "";
        
        //Fer el canvi de - a lletra
        for (int i = 0; i < secretWord.length(); i++){
            c2 = secretWord.charAt(i);
            if(c2 == c){
                tal[i] = c;
            }
        }
        
        //Transformar larray a string
        for(int i = 0;i < tal.length; i++){  
            algo = algo + tal[i];  
        } 
        
        return algo;
    }

    /**
     * Main program.
     * 
     * @param args Not used.
     */
    public static void main(String[] args) {
        Program2 p = new Program2();
        Scanner sc = new Scanner(System.in);
        Random r = new Random();
        String[] words = { "gat", "gos", "gallina", "vaca" };
        int maxGuesses = 10;
        boolean win = false;
        int intents = 0;
        String lletra;
        
        int secretPosition = r.nextInt(words.length);
        String secretWord = words[secretPosition];
        String hiddenWord = p.hide(secretWord);
        
        System.out.println("JOC DEL PENJAT");
        System.out.println("==============\n");
        System.out.println(hiddenWord);
        
        do{
            System.out.printf("Lletra? ");
            lletra = sc.nextLine();
            hiddenWord = p.refresh(secretWord,hiddenWord,lletra);
            System.out.println(hiddenWord);
            
            if(hiddenWord.indexOf("-") == -1){
                win = true;
                System.out.printf("Has guanyat amb %d intents.\n",intents + 1);
                intents++;
            } else {
                intents++;
            }
        } while (!win && intents < maxGuesses);
        
        if(!win){
            System.out.printf("Has perdut. La paraula secreta era: %s\n",secretWord);
        }
    }

}
