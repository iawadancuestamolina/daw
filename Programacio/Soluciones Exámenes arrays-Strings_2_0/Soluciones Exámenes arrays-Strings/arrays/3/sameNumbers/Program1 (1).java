/*
 * Program.java        1.0 10/01/2013
 *
 * Models the program.
 *
 * Copyright 2013 NOM I COGNOMS <MAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program1 {

    /**
     * Determines if two arrays have the same elements.
     * 
     * @param a an array
     * @param b an array
     * @return true if they have the same elements, false otherwise.
     */
    public boolean sameNumbers(int[] a, int[] b) {
        boolean iguals = false;
        
        if (a.length == b.length){
            for (int i = 0; i < a.length; i++){
                for (int j = 0; j < b.length; j++){
                    if(a[i] == b[j]){
                        iguals = true;
                    }
                }
            }
        }
        
        return iguals;
    }
}
