/*
 * Program.java        1.0 09/01/2012
 *
 * Models the program.
 *
 * Copyright 2012 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program3 {
    
    /**
     * Convert a matrix into a vector.
     * 
     * @param matrix a matrix of integer numbers.
     * @return a vector with the elements of the matrix in a row.
     */
    public int[] matrix2vector(int[][] matrix) {
        int k = 0;
        int[] a = new int[matrix.length * matrix[0].length];
        for(int i = 0; i < matrix.length; i++){
            for(int j = 0; j < matrix[i].length; j++){
                a[k] = matrix[i][j];
                k++;
            }
        }
        
        return a;
    }
}
