/*
 * Program.java        1.0 09/01/2012
 *
 * Models the program.
 *
 * Copyright 2012 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program2 {

    /**
     * Finds out which team is the winner.
     * 
     * @param wonMatches an array storing how many matches has won each team.
     * @param goalsScored an array storing how many goals has scored each team.
     * @return the code of the winner team.
     */
    public int footballWinner(int[] wonMatches, int[] goalsScored) {
        int winner = 0;
        for (int i = 0; i < wonMatches.length; i++){
            if (wonMatches[i] > wonMatches[winner]){
                winner = i;
            } else if (wonMatches[i] == winner){
                for(int j = 0; j < goalsScored.length; j++){
                    if(goalsScored[j] >= goalsScored[winner]){
                        winner = j;
                    }
                }
            }
        }
        
        return winner;
    }
}
