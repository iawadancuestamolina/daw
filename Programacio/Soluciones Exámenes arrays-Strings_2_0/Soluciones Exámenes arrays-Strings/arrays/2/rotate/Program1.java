/*
 * Program.java        1.0 09/01/2012
 *
 * Models the program.
 *
 * Copyright 2012 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program1 {

    /**
     * Rotates one position in a matrix forward or backwards.
     * 
     * @param a an array of integer numbers.
     * @param forward true if we want to rotate forward, false if we want to
     *        rotate backwards.
     */
    public int[] rotate(int[] a, boolean forward) {
        int aux;
        int[] a2 = new int[a.length];
        if(forward){
            aux = 1;
            a2[0] = a[a.length - 1];
            for (int i = 0; i < a.length - 1; i++){
                a2[aux] = a[i];
                aux++;
            }
        }else{
            aux = 1;
            a2[a.length - 1] = a[0];
            for (int i = 0; i < a.length - 1; i++){
                a2[i] = a[aux];
                aux++;
            }
        }
        return a2;
    }
}
