/*
 * Exercici2.java        1.0 24/01/2011
 *
 * Fer un programa que donat un vector v de n nombres enters 
 * i un nombre p positiu i <=n, crei un vector vse de n-1 nombres enters 
 * que tingui els mateixos elements que el vector v excepte l'element de la 
 * p-èsima posició.
 * 
 * Per exemple:
 * 
 *  - Si v = {7, 15, 5, 8, 9} i p = 2, llavors vse = {7, 5, 8, 9}
 *  - Si v = {7, 15, 5, 8, 9} i p = 3, llavors vse = {7, 15, 8, 9}
 *  - Si v = {7, 15, 5, 8, 9} i p = 1, llavors vse = {15, 5, 8, 9}  
 *  - Si v = {7, 15, 5, 8, 9} i p = 5, llavors vse = {7, 15, 5, 8}
 *
 * Copyright 2010 NOM COGNOMS WIDA <MAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Exercici2 {
    
    public double[] vectorWithoutelement(double[] v, int p) {
        double[] v2 = new double[v.length - 1];
        int aux = 0;
        
        for (int i = 0; i < v.length; i++){
            if (i != p - 1){
                v2[aux] = v[i];
                aux++;
            }else{
                v2[aux] = 0;
            }
        }
        return v2;
    }
    
}
