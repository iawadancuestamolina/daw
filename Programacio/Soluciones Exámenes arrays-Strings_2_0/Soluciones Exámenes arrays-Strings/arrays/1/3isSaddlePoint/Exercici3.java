/*
 * Exercici3.java        1.0 24/01/2011
 *
 * Un nombre en una matriu és un "punt de sella" si és el mínim nombre de la 
 * seva fila i el màxim nombre de la seva columna.
 * 
 * Fer un programa que donada una matriu de nombres enters m, i 
 * dos nombres enters no negatius r i c, determini si el nombre situat 
 * a la fila r i columna c és un "punt de sella".
 * 
 * Per exemple, donada la següent matriu:
 * 
 * 1 2 3
 * 4 5 9
 * 2 8 6
 * 1 0 7
 * 
 * - Si r = 1 i c = 0, m[1][0] és 4 i és un punt de sella
 * - Si r = 2 i c = 2, m[2][2] és 6 i NO és un punt de sella
 * - Si r = 0 i c = 0, m[0][0] és 1 i NO és un punt de sella
 * - Si r = 1 i c = 1, m[1][2] és 9 i NO és un punt de sella
 *
 * Copyright 2010 NOM COGNOMS WIDA <MAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Exercici3 {
    
    public boolean isSaddlePoint(int[][] m, int r, int c) {
        int min = m[r][0], max = m[0][c], n;
        boolean algo = false;
        
        //max de la columna
        for (int i = 0; i < m.length; i++){
            n = m[i][c];
            if(n >= max){
                max = n;
            }
        }
        
        //min de la fila
        for (int i = 0; i < m[r].length; i++){
            n = m[r][i];
            if (n <= min){
                min = n;
            }
        }
        
        return m[r][c] == min && m[r][c] == max;
    }
}
