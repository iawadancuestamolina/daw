/*
 * Exercici1.java        1.0 24/01/2011
 *
 * Un matemàtic avorrit s'inventa l'operació "producte invertit de 2 vectors" 
 * definida de la següent manera: 
 * 
 * Donats dos vectors v1 i v2 de dimensió n, el seu producte invertit és 
 * la suma dels següents productes: 
 *  - el 1er terme de v1 multiplicat pel terme n de v2
 *  - el 2on terme de v1  multiplicat pel terme n-1 de v2
 *  -...
 *  - el terme n de v1 multiplicat pel 1er terme de v2
 * 
 * Per exemple:
 *  - Si v1 = {1, 2, 3} i v2 = {4, 5, 6}, 
 *    el seu producte invertit és 1 * 6 + 2 * 5 + 3 * 4 (=28)
 *  - Si v1 = {1, 2, 3, 4, 5} i v2 = {6, 7, 8, 9, 10}, 
 *    el seu producte invertit és 1 * 10 + 2 * 9 + 3 * 8 + 4 * 7 + 5 * 6 (=110)  
 * 
 * Fer un programa que donats dos vectors de la mateixa dimensió, 
 * calculi el seu "producte invertit".
 *
 * Copyright 2010 NOM COGNOMS WIDA <MAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */


public class Exercici1 {
    
    public double inverseProduct(double[] v1, double[] v2) {
        double[] aux = new double[v1.length];
        double prod = 0;
        
        for (int i = 0; i < v1.length; i++){
            aux[i] = v1[i] * v2[v2.length - 1 - i];
            prod += aux[i];
        }
        return prod;   
    }
    
}
