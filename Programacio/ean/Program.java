/*
 * Program.java        1.0 23/01/2012
 *
 * Models the program.
 *
 * Copyright 2012 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Determines if an EAN code is valid.
     * 
     * @param ean an EAN code
     * @return true if it is valid, false otherwise
     */
    public int isValidEAN(String s) {
        //inicializamos variables
        String reverse = "";
        int longitud = s.length();
        int control = Integer.valueOf(s.charAt(longitud - 1));
        int numero;
        int pares = 0;
        int impares = 0;
        int sum;
        boolean valido = false;
        
        //giramos el codigo quitando el último número
        longitud = longitud-1;
        s = s.substring(0,longitud);
        
        for (int i = longitud-1; i >= 0; i--) {
            reverse = reverse + s.charAt(i);
        }
        //sumamos los numeros impares y multiplicamos el resultado por 3
        for (int i = 0; i < longitud; i++) {
            numero = Integer.parseInt(String.valueOf(reverse.charAt(i)));
            if (numero%2!=0) {
                impares += numero;
            }
        }
        impares = impares*3;
        //sumamos los números pares y lo sumamos todo
        for (int i = 0; i < longitud; i++) {
              numero = Integer.parseInt(String.valueOf(reverse.charAt(i)));
            if (numero%2 == 0) {
                pares += numero;
            }
        }
        sum = pares + impares;
        //sacamos la fórmula (10-sum%10)%10
        sum = (10-sum%10)%10;
        //comprobamos que sum y control son iguales
        if (sum == control) {
            valido = true;
        }
        return pares;
    }
}
