/*
 * Program.java        1.0 09/01/2012
 *
 * Models the program.
 *
 * Copyright 2012 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Finds out which team is the winner.
     * 
     * @param wonMatches an array storing how many matches has won each team.
     * @param goalsScored an array storing how many goals has scored each team.
     * @return the code of the winner team.
     */
    public int footballWinner(int[] wonMatches, int[] goalsScored) {
        //inicializamos variables
        int ganador = 0;
        int partidos = 0;
        int goles = 0;
        int longitud = wonMatches.length;
        //recorremos los arrays comprobando los partidos y goles
        for (int i = longitud-1; i >= 0; i--) {
            if (wonMatches[i] > partidos) {
                ganador = i;
                partidos = wonMatches[i];
            } else if (wonMatches[i] == partidos) {
                if (goalsScored[i] >= goles) {
                    goles = goalsScored[i];
                    ganador = i;
                }
            }
        }
        return ganador;
    }
}
