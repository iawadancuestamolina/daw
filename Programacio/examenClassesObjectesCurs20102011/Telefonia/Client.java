/*
 * Client.java        1.0 17/03/2011
 *
 * Modela un client d'una companyia de telefonia.
 *
 * Copyright 2011 NOM I COGNOMS <MAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import library.utilitats.JodaDT;

import org.joda.time.DateTime;

public class Client {

	/** Anys d'antiguitat per ser VIP per antiguitat */
	private static final int ANYS_ANTIGUITAT_VIP = 10;
	/** Nom del client */
	private String nom;
	/** Codi identificador del client */
	private long codi;
	/** Preu per segon que se li aplicarà a les trucades del client */
	private double tarifaSegon;
	/** Si és VIP o no */
	private boolean vip;
	/** Data d'inici del contracte en el format DD/MM/AAAA */
	private String dataContracte;

	// Constructors

	// Mètodes

	/**
	 * Estableix el client com a client VIP si porta més de ANYS_ANTIGUITAT_VIP anys com a client.
	 * 
	 * @return true si s'ha establert el client com a client VIP, false en cas contrari
	 */
	public boolean vipPerAntiguitat() {
		// TODO
	}

	// Getters i setters

}
