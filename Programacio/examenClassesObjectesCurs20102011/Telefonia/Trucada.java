/*
 * Trucada.java        1.0 15/03/2011
 *
 * Modela una trucada telefònica.
 *
 * Copyright 2011 NOM I COGNOMS <MAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import library.inout.Terminal;
import library.utilitats.JodaDT;

import org.joda.time.DateTime;
import org.joda.time.Period;
import java.time.LocalDateTime;

public class Trucada {

 /** Percentatge de descompte */
 private static final double PERCENTATGE_DESCOMPTE = 5;
 /** Nombre d'hores a partir de les quals es considera que una trucada és llarga */
 private static final double HORES_TRUCADA_LLARGA = 3;
 /** Client que realitza la trucada */
 private Client cli;
 /** Data i hora en la qual es comença la trucada en el format DD/MM/AAAA-hh:mm:ss */
 private String dataInici;
 /** Data i hora en la qual es finalitza la trucada en el format DD/MM/AAAA-hh:mm:ss */
 private String dataFi;

 // Constructors

 // Mètodes

 /**
  * Calcula el període entre la data i hora d'inici de la trucada i la data i hora final.
  * 
  * @return El període entre les dues dates i hores.
  */
 private Period periodeTrucada() {
  // TODO
     
 }

 /**
  * Calcula el temps de la durada de la trucada en el format hh:mm:ss.
  * 
  * @return L'String en format "hh:mm:ss".
  */
 public String tempsTrucada() {
  // TODO
 }

 /**
  * Calcula el nombre de segons de la trucada.
  * 
  * @return El nombre de segons.
  */
 public int nombreSegons() {
  // TODO
 }

 /**
  * Esbrina si s'aplicarà un descompte per haver realitzat una trucada llarga. El descompte per
  * trucada llarga s'aplica quan la trucada ha durat més de HORES_TRUCADA_LLARGA hores.
  * 
  * @return true si s'aplica el descompte, false altrament.
  */
 private boolean descomptePerTrucadaLlarga() {
  // TODO
 }

 /**
  * Estableix si s'aplica descompte o no a aquesta trucada. S'aplicarà descompte en els dos casos
  * següents:
  * 
  * 1.- Si el client és client VIP. S'ha de tenir en compte que si el client és VIP per
  * antiguitat i encara no se l'ha establert com a client VIP, ho haurem de fer des d'aquest
  * mètode.
  * 
  * 2.- Si es fa descompte per trucada llarga.
  * 
  * @return true si s'aplica, false si no s'aplica.
  */
 public boolean aplicaDescompte() {
  // TODO
 }

 /**
  * Calcula el preu final de la trucada aplicant descomptes, si s'escau.
  * 
  * @return el preu de la trucada en € i cèntims d'€ (només dues xifres decimals)
  */
 public double preuTrucada() {
  // TODO
 }

 /**
  * Imprimeix un informe amb un resum de la trucada efectuada. El format de l'informe és el
  * següent:
  * 
  * Si és un client VIP:
  * 
  * INFORME TRUCADA
  * ===============
  * 
  * Client : 123 - Maria Roca - VIP
  * Data Inici: 01/02/2010-13:00:00
  * Data Fi : 01/02/2010-13:05:00
  * Segons : 300
  * Preu : 142.5
  * 
  * Si NO és un client VIP:
  * 
  * INFORME TRUCADA
  * ===============
  * Client : 124 - Pep Mir
  * Data Inici: 01/02/2010-13:00:00
  * Data Fi : 01/02/2010-13:05:05
  * Segons : 305
  * Preu : 152.5
  */
 public void informeTrucada() {
  // TODO
 }

}
