/*
 * Program.java        1.0 23/01/2012
 *
 * Models the program.
 *
 * Copyright 2012 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Translates a word from spanish to jerigonza.
     * 
     * @param s a word
     * @return the translated word
     */
    public String spanish2jerigonza(String s) {
        //inicializamos las variables
        char vocal;
        int longitud = s.length();
        int nuevoInicio = 0;
        String vocales = "aeiou";
        String nuevaFrase = "";
        
        //recorremos String comprobando si es o no una vocal de un string de vocales
        for (int i = 0; i < longitud; i++) {
            for (int j = 0; j < 5; j++) {
                vocal = vocales.charAt(j);//si es una vocal se almacena en la variable vocal
                if (s.charAt(i) == vocal) {
                    nuevaFrase = nuevaFrase + s.substring(nuevoInicio,i+1) + "p" + vocal; //se añade después de la vocal con substring()
                    nuevoInicio = i+1;
                    
                }
            }
        }
        nuevaFrase = nuevaFrase + s.substring(nuevoInicio, longitud);

        return nuevaFrase;
        
    }

    /**
     * Translates a word from english to basic pig latin.
     * 
     * @param s a word
     * @return the translated word
     */
    public String english2basicPigLatin(String s) {
        //inicializamos variables incluyendo String vocales
        int longitud = s.length();
        String vocales = "aeiou";
        String nuevaFrase = "";
        boolean vocal = false;
        //recorremos el String comprobando si es vocal o no cada letra
        int i = 0;
        do {
            if (s.charAt(i)=='a' ||  s.charAt(i)=='e' || s.charAt(i)=='i' || s.charAt(i)=='o' || s.charAt(i)=='u') {
                vocal = true;
            };
            i++;
         } while (vocal != true);
         
        //si es vocal no hacemos nada y añadimos "ay" al final
         if (vocal==true) {
             nuevaFrase = s.substring(i-1,longitud) + s.substring(0,i-1) + "ay"; 
             //si es consonante buscamos la primera vocal y cortamos en substring poniéndolo al final con concat() + ay
         }
         return nuevaFrase;
        
    }
}
