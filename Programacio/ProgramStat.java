/*
 *  Program.java         1.0 19/1/2021
 * 
 * Copyright 2021 Adán Cuesta Molina <1hiawcuestamolinaadan@gmail.com>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information
 */
public class ProgramStat{
 /**
   Un alumne de DAW desitja realitzar una estadística de les hores d'estudi mensuals dedicades 
   a cadascuna de les seves assignatures. Fer un mètode que ens permeti calcular:

El total anual d'hores dedicades a cada assignatura
El total mensual d'hores dedicades a estudiar

El mètode rebrà com a paràmetre un array bidimensional on les files representen les
assigantures i les columnes les hores estudiades cada mes per a cada assignatura. El
mètode haurà d'escriure la següent taula per pantalla:
   * 
   * @param a un array bidimensional

   * @return 
   */
    int[][] a = {{1,1,1,1,1,1,1,1,1,1,1,1},{1,1,1,1,1,1,1,1,1,1,1,1},{1,1,1,1,1,1,1,1,1,1,1,1},
        {1,1,1,1,1,1,1,1,1,1,1,1},{1,1,1,1,1,1,1,1,1,1,1,1},{1,1,1,1,1,1,1,1,1,1,1,1}};
    
    public void estudio(int[][]a){
    
    //inicialización de variables
        String meses= "En  |Feb |Mar |Ab  |May |Jun |Jul |Ag  |Sep |Oct |Nov |Di  |TOTAL";
        int[][] tabla = new int[7][13];
        //recorrido para llenar el array
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                tabla[i][j]=a[i][j];
            }
        }
        
         for (int i = 0; i < tabla.length - 1; i++) {
            for (int j = 0; j < tabla[0].length  - 1; j++) {
                tabla[i][tabla[i].length - 1] += tabla[i][j];
                tabla[tabla.length-1][j] += tabla[i][j];
            }
         }
        
         for (int i = 0; i < tabla[6].length -1; i++) {
           
             tabla[6][12]+= tabla[6][i];
         }
         
        //recorrido para imprimirlo
        for (int i = 0; i < tabla.length; i++) {
            for (int j = 0; j < tabla[0].length; j++) {
                System.out.print(tabla[i][j]);       
            }
            System.out.println();
        }
    }
}