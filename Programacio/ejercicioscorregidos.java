/*
 *  Program. java         1.0 26/10/2020
 * 
 * Copyright 2020 Sergio Hortas Lijó <1hiaw.hortaslijosergio@gmail.com>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information
 */
 
public class Program {
                        
   /**
   * Calculate the perimeter of a circle
   *  
   * @param radius an double number to be added
   * @param diameter an double number to be added
   * @param Pi is a Math.Pi
   * @return the circunference multiplying pi by diameter
   */
   public double perimeter(double radius)  {
    double perimeter;
    perimeter = 2 * Math.PI * radius ;
    return perimeter;
  
   }
   /**
   * Calculate the area of a circle
   *  
   * @param radius an double number to be added
   * @return the area multiplying Math.PI * (radius*radius)
   */
   
   
   public double area (double radius)   {
     double area;
     area = Math.PI * (radius * radius);
     return area;
  }
}        