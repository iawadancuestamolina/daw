/*
 * ProgramTUI.java        1.0 07/11/2012
 *
 * Models the program.
 *
 * Copyright 2012 NOM I COGNOMS <MAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Locale;
import java.util.Scanner;

public class ProgramTUI {

    /**
     * Generates a random card.
     * 
     * @return a random number between 1 and 12.
     */
    public int randomCard() {
        return (int) (Math.random() * 12) + 1;
    }

    /**
     * Calculates the sum of 3 cards. The value for cards 8, 9, 10, 11 and 12 is 0.5.
     * 
     * @param c1 a card (an integer number between 1 and 12)
     * @param c2 a card (an integer number between 1 and 12)
     * @param c3 a card (an integer number between 1 and 12)
     * @return the sum.
     */
    public double result(int c1, int c2, int c3) {
        // TODO
        double card1, card2, card3, value;
        if (c1>=8){
        card1=c1*0.5;
        }else{
        card1=c1;
        }
         if (c2>=8){
        card2=c2*0.5;
        }else{
        card2=c2;
        }
         if (c3>=8){
        card3=c3*0.5;
        }else{
        card3=c3;
        }
        value = card1+card2+card3;
        
        return value;
    }

    /**
     * Calculates the amount of money a player has after a game.
     * 
     * @param resultGame the sum of the three cards
     * @param money the amount of money the player has initially.
     * @param bet the amount of money the player is betting in the game.
     * @return the final amount of money the player has after the game, depending on the result of the game.
     */
    public double prize(double resultGame, double money, double bet) {
        // TODO
        if (resultGame==7.5){
        money=money+2*bet;
        }else if(resultGame<7.5){
        money=money;
        }else {
        money=money-bet;
        }
        
        return money;
    }

    /**
     * Game TUI.
     * 
     * @param args Not used.
     */
    public static void main(String[] args) {
        // TODO
        int carta1, carta2, carta3, dinero, apuesta, dineroPost;
        double resultado;
        
        System.out.println("EL GRAN JUEGO DEL SIETE Y MEDIOOOO!");
        ProgramTUI p = new ProgramTUI();
        Scanner s = new Scanner(System.in);
        
          System.out.print("Cuánto dinero tienes?");
                dinero=s.nextInt(); 
                System.out.print("Cuánto apuestas?");
                apuesta=s.nextInt();
        
        carta1=p.randomCard();
                System.out.printf("Tu primera carta es: %d \n", carta1);
                 carta2=p.randomCard();
                System.out.printf("Tu segunda carta es: %d \n", carta2);
                 carta3=p.randomCard();
                System.out.printf("Tu tercera carta es: %d \n", carta3);
                
               resultado=p.result(carta1, carta2, carta3);
              dineroPost=(int)p.prize(resultado, dinero, apuesta);
              
                
                //hacemos el programa y sacamos el dinero
                
              if(dineroPost<=dinero){
              System.out.printf("Partida perdida! Te has quedado con %d dinero!", dineroPost);
              }else{
              System.out.printf("Partida ganada! Te has quedado con %d dinero!", dineroPost);
              }
    }

}
