/*
 * Program.java    1.0 13/01/2021
 *
 * Un alumne de DAW desitja realitzar una estadística de les hores d'estudi mensuals dedicades 
 * a cadascuna de les seves assignatures. Fer un mètode que ens permeti calcular:
 *     - El total anual d'hores dedicades a cada assignatura
 *     - El total mensual d'hores dedicades a estudiar
 *
 * Copyright 2020 Keila Gonzalez <1hiawgonzalezkeila@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program4 {
    
    /**
     * Un alumne de DAW desitja realitzar una estadística de les hores d'estudi mensuals dedicades 
     * a cadascuna de les seves assignatures. Fer un mètode que ens permeti calcular:
     *     - El total anual d'hores dedicades a cada assignatura
     *     - El total mensual d'hores dedicades a estudiar
     * 
     * @param m, matriu de numeros reals
     */
    
    public void statistics(int m[][]){
        int[][] mTotal = new int[m.length + 1][m[0].length + 1];
        String[] mesos = {"","Gen","Feb","Mar","Abr","Mai","Jun","Jul","Ago","Set","Oct","Nov","Dec","total"};
        String[] assignatures = {"SO","PROG","BBDD","XML","FOL","EIE","total"};
        
        //nou array amb les notes i una fila/columna mes pels totals
        for(int i = 0; i < m.length; i++){
            for(int j = 0; j < m[0].length; j++){
                mTotal[i][j] = m[i][j];
            }
        }
        
        //suma dels totals
        for(int i = 0; i < mTotal.length - 1; i++){
            for(int j = 0; j < mTotal[0].length - 1; j++){
                mTotal[i][mTotal[i].length - 1] += mTotal[i][j];
                mTotal[mTotal.length - 1][j] += mTotal[i][j];
            }
            
        }
        
        //suma anual
        for(int i = 0; i < mTotal.length - 2; i++){
            for(int j = 0; j < mTotal[i].length - 1; j++){
                mTotal[mTotal.length - 1][mTotal[0].length - 1] += mTotal[mTotal.length - 1][j];
            }   
        }
        
        //posar els mesos
        for(int i = 0; i < mesos.length; i++){
            System.out.printf(" |%2s ",mesos[i]);
        }
        System.out.println();
        
        //imprimir taula
        for(int i = 0; i < mTotal.length; i++){
            System.out.printf("%-5s",assignatures[i]);
            for(int j = 0; j < mTotal[0].length; j++){
                System.out.printf(" | %-3d",mTotal[i][j]);
            }
            System.out.println();
        }        
    }
}