/*
 * Player.java        1.0 22/02/2012
 *
 * Models the program.
 *
 * Copyright 2012 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 *                Joaquim Laplana Tarragona <jlaplana@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.time.LocalDateTime;

public class Player {

    /** Nom del jugador */
    private String name;
    /** Data de naixement del jugador */
    private LocalDateTime birthday;
    /** Data de mort del jugador */
    private LocalDateTime deathday;
    /** Targeta de "goodies" del jugador */
    private GoodiesCard goodiesCard;
    /** Temps de vida inicial d'un jugador, en minuts */
    private static final int INITIAL_TIME_OF_LIFE = 5;
    /** Temps de vida pel qual es pot intercanviar un superpoder, en minuts */
    private static final int SUPER_POWER_LIFE_VALUE = 10;
    /** Quilocalories diàries que necessita un jugador */
    private static final double KCAL_PER_DAY = 2200;
    /** Preu d'1 quilocaloria */
    private static final int KCAL_PRICE = 2;
    /** Valor en punts d'alguns "goodies" */
    private static final int EXTRAS_VALUE = 15;

    /**
     * Constructor de la classe Player. Quan es crea un jugador es complirà: 
     *   - La data de naixement és l'hora actual. 
     *   - La data de mort és d'aquí a INITIAL_TIME_OF_LIFE minuts. 
     *   - Se li crearà una targeta de "goodies" per defecte.
     * 
     * @param name el nom del jugador
     */
    public Player(String name) {
        // TODO
        this.name = name;
        this.birthday = LocalDateTime.now();
        this.deathday = birthday.plusMinutes(this.INITIAL_TIME_OF_LIFE);
        GoodiesCard goodiesCard = new GoodiesCard();
        this.setGoodiesCard(goodiesCard);
        
    }

    /**
     * Calcula el temps de vida del jugador, des de que neix fins que mor.
     * 
     * @return la quantitat de segons de vida del jugador.
     */
    public long timeOfLife() {
        // TODO
        return JodaDT.durationInSeconds(this.birthday, this.deathday);
    }

    /**
     * Determina si el jugador està viu.
     * 
     * @return true si està viu, false altrament
     */
    public boolean isAlive() {
        // TODO
        return JodaDT.isInInterval(LocalDateTime.now(),this.birthday,this.deathday);//si el instante actual está entre el nacimiento y la muerte, está vivo
    }

    /**
     * Calcula quant temps de vida li queda al jugador. Si el jugador està
     * mort no té temps de vida.
     * 
     * @return la quantitat de segons de vida que li queden al jugador.
     */
    public long restOfLife() {
        // TODO
        long segundos = JodaDT.durationInSeconds(LocalDateTime.now(),this.deathday);
        if (segundos <= 0) {
            return 0;
        } else {
            return segundos;
        }
    }

    /**
     * Intercanvia -n- superpoders per temps de vida. Cada superpoder val
     * SUPER_POWER_LIFE_VALUE minuts de vida.
     * 
     * Per tal de fer l'intercanvi s'ha de comprovar que: 
     *   - el jugador està viu
     *   - el jugador disposa com a mínim de -n- superpoders
     * 
     * @param n nombre de superpoders per intercanviar
     * @return true si s'ha fet l'intercanvi, false altrament
     */
    public boolean superPower2life(int n) {
        // TODO
        int poderes = this.getGoodiesCard().getSuperPower();
        if (this.isAlive() == true && poderes > 0) {
            this.setDeathday(this.getDeathday().plusMinutes(n*this.SUPER_POWER_LIFE_VALUE));
            this.getGoodiesCard().setSuperPower(poderes - n);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Calcula la quantitat de quilocalories que necessita un jugador des d'ara
     * fins el dia de la seva mort. Si el jugador està mort, no necessita
     * quilocalories.
     * 
     * @return el nombre quilocalories
     */
    public double kcalNeeded() {
        // TODO
        double kcalXSec = this.KCAL_PER_DAY/86400;
        return this.restOfLife()*kcalXSec;
    }

    /**
     * Compra -food- quilocalories (amb diners).
     * 
     * Per tal de fer l'intercanvi s'ha de comprovar que: 
     *   - el jugador està viu
     *   - el jugador disposa dels diners necessaris per comprar les quilocalories
     * demanades.
     * 
     * @param food quantitat de quilocalories que vol comprar
     * @return true si s'ha efectuat la compra, false altrament
     */
    public boolean buyFood(long food) {
        // TODO
        double precio = food*this.KCAL_PRICE;
        double dineros = this.getGoodiesCard().getMoney();
        
        if (this.isAlive() == true &&  precio <= dineros) {
            this.goodiesCard.setMoney(dineros - precio);
            this.goodiesCard.setFood(this.goodiesCard.getFood()+food);
            return true;
        } else {
        return false;
        }
    }

    /**
     * Intercanvia menjar per salut. Per aconseguir el 'goodie' de salut, el
     * jugador necessita donar les quilocalories que necessita per viure el que
     * li queda de vida.
     * 
     * Per tal de fer l'intercanvi s'ha de comprovar que: 
     *   - el jugador està viu
     *   - el jugador disposa de les quilocalories necessàries per aconseguir el
     * 'goodie' de la salut.
     * 
     * @return true si s'ha aconseguit el goodie salut, false altrament
     */
    public boolean food2health() {
        // TODO
        double calorias = this.goodiesCard.getFood();
        double caloriasNecesarias = this.kcalNeeded();
        if (this.isAlive() == true && calorias >= caloriasNecesarias) {
            this.getGoodiesCard().setHealth(true);  
            this.getGoodiesCard().setFood(calorias - caloriasNecesarias);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Perd els següents goodies: 
     *   - salut 
     *   - amor 
     *   - tots els diners 
     *   - tots els superpoders
     */
    public void looseSomeGoodies() {
        // TODO
        this.goodiesCard.setHealth(false);
        this.goodiesCard.setLove(false);
        this.goodiesCard.setMoney(0);
        this.goodiesCard.setSuperPower(0);
    }

    /**
     * Guanya diners.
     * 
     * @param money nombre d'euros que s'acumulen als que ja té el jugador.
     */
    public void winMoney(double money) {
        // TODO
        this.goodiesCard.setMoney(this.goodiesCard.getMoney()+money);
    }

    /**
     * El jugador mor ara mateix.
     */
    public void gameover() {
        // TODO
        this.deathday = LocalDateTime.now();
      
    }

    /**
     * Calcula el nombre de punts del jugador. Els punts es calculen de la
     * següent manera: 
     *   - Cada superpoder val 1 punt 
     *   - L'amor i la salut valen Player.EXTRAS_VALUE punts 
     *   - Si no es té salut es resten Player.EXTRAS_VALUE punts 
     *   - Els diners donen tant punts com euros es tinguin sense decimals 
     *   (truncant a la baixa)
     * 
     * NO es pot retornar un valor negatiu. Si tenim punts negatius, es
     * considera que tenim 0 punts.
     * 
     * @return el nombre de punts.
     */
    public int points() {
        // TODO
        int puntos = 0;
        int extraValue = this.EXTRAS_VALUE;
        
        puntos += this.getGoodiesCard().getSuperPower();
        if (this.goodiesCard.getHealth() == true) {
            puntos += extraValue;
        } else {
            puntos -= extraValue;
        }
        
        if (this.goodiesCard.getLove() == true) {
            puntos += extraValue;
        } 
        puntos += (int) this.goodiesCard.getMoney();
        
        if (puntos < 0) {
            return 0;
        } else {
            return puntos;
        }
        
        
        
    }

    /**
     * Determina quin jugador mor abans.
     * 
     * @param anotherPlayer un altre jugador
     * @return el jugador que morirà abans
     */
    public Player whoDiesBefore(Player anotherPlayer) {
        // TODO
        if (JodaDT.isInInterval(this.getDeathday(),LocalDateTime.now(),anotherPlayer.getDeathday()) == true) {
            return this;
        } else {
            return anotherPlayer;
        }
    }

    /**
     * Determina quin jugador viu més temps.
     * 
     * @param anotherPlayer un altre jugador
     * @return el jugador que viu més temps
     */
    public Player whoLivesMore(Player anotherPlayer) {
        // TODO
        if (JodaDT.durationInSeconds(this.getBirthday(),this.getDeathday()) > 
            JodaDT.durationInSeconds(anotherPlayer.getBirthday(),anotherPlayer.getDeathday())) {
            return this;
        } else {
            return anotherPlayer;
        }
    }

    /**
     * Genera un informe del jugador amb el següent format:
     * 
     * JUGADOR: Laura 
     * ================= 
     * Data de naixement: 23/02/2012-21:12 
     * Data de mort: 23/02/2012-21:17 
     * Temps de vida restant (mm:ss): 4:27 
     * Salut: no
     * Amor: sí 
     * Diners: 100.0€ 
     * Menjar: 1000.0kcal 
     * Superpoders: 3 
     * Punts: 118
     * 
     */
    public void reportStatusPlayer() {
        // calculamos tiempo de vida en minutos y sec
        long tiempoRestanteMin = this.restOfLife()/60;
        long tiempoRestanteSec = this.restOfLife()%60;
        //calculamos string para salut
        String salud;
        if (this.goodiesCard.getHealth() == true) {
            salud = "sí";
        } else {
            salud = "no";
        }
        //calculamos string para amor
        String amor;
        if (this.goodiesCard.getLove() == true) {
            amor = "sí";
        } else {
            amor = "no";
        }
        
        //imprimimos
        System.out.printf("JUGADOR: %s\n",this.name);
        System.out.println("======================");
        System.out.println("Data de naixement: " + JodaDT.formatDDMMYYYYhhmmss(this.getBirthday()));
        System.out.println("Data de mort: " + JodaDT.formatDDMMYYYYhhmmss(this.getDeathday()));
        System.out.println("Temps de vida restant (mm:ss): " + tiempoRestanteMin + ":" + tiempoRestanteSec);
        System.out.println("Salut: " + salud);
        System.out.println("Amor: " + amor);
        System.out.printf("Diners: %.1f€\n", this.goodiesCard.getMoney());
        System.out.printf("Menjar: %.1fkcal\n", this.getGoodiesCard().getFood());
        System.out.printf("Superpoders: %d \n", this.getGoodiesCard().getSuperPower());
        System.out.printf("Punts: %d \n", this.points());
        

    }

    // Getters & setters
    // Codifiqueu els que necessiteu
    
    //TODO
    public void setGoodiesCard(GoodiesCard goodiesCard) {
        this.goodiesCard = goodiesCard;
    }
    
    public GoodiesCard getGoodiesCard() {
        return this.goodiesCard;
    }
    
   
     public LocalDateTime getBirthday() {
        return this.birthday;
    }
    
    public void setBirthday(LocalDateTime birthday) {
        this.birthday = birthday;
    }
    
    public LocalDateTime getDeathday() {
        return this.deathday;
    }
    
    public void setDeathday(LocalDateTime deathday) {
        this.deathday = deathday;
    }
}
