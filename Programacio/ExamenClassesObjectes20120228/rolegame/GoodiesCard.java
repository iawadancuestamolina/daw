/*
 * Bonus.java        1.0 22/02/2012
 *
 * Models the program.
 *
 * Copyright 2012 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 *                Joaquim Laplana Tarragona <jlaplana@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class GoodiesCard {
    /** Nombre de superpoders */
    private int superPower;
    /** Quantitat de diners, en euros */
    private double money;
    /** Es té amor o no */
    private boolean love;
    /** Es té salut o no */
    private boolean health;
    /** Quantitat de menjar en quilocalories */
    private double food;

    /**
     * Constructor de la classe GoodiesCard, per defecte. 
     * En crear una carta de "goodies" per defecte es tindrà: 
     *   - 3 superpoders 
     *   - 100€ 
     *   - tindrà amor
     *   - NO es tindrà salut 
     *   - 1000 quilocalories
     */
    public GoodiesCard() {
        // TODO
        this.superPower = 3;
        this.money = 100;
        this.love = true;
        this.health = false;
        this.food = 1000;
    }

    // Getters & setters
    // Codifiqueu els que necessiteu

    // TODO
       public int getSuperPower() {
           return this.superPower;
    }
       
       public void setSuperPower(int n) {
           this.superPower = n;
       } 
       
       public double getMoney() {
           return this.money;
       }
       
       public void setMoney(double money) {
           this.money = money;
       }
       
        public double getFood() {
           return this.food;
       }
       
       public void setFood(double food) {
           this.food = food;
       }
       
       public boolean getHealth() {
           return this.health;
       }
       
       public void setHealth(boolean health) {
           this.health = health;
       }
       
        public boolean getLove() {
           return this.love;
       }
       
       public void setLove(boolean love) {
           this.love = love;
       }
}
