/*
 *  Program. java         1.0 26/10/2020
 * 
 * Copyright 2020 Adán Cuesta Molina <1hiawcuestamolinaadan@gmail.com>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information
 */
 
public class Program {
                        
   /**
   * Add two real numbers
   *  
   * @param a a real number
   * @param b a real number
   * @return the sum of -a- and -b-
   */
   public double add(double a, double b)  {
    double suma;
    suma = a + b;
    return suma;
   }
   
   /**
   * Multiply two real numbers
   * 
   *  @param a a real number
   *  @param b a real number
   *  @return the multiplication of -a- and -b-
   */
   public double multiply(double a, double b) {
     double multiply;
     multiply = a * b;
     return multiply;
   }

    /** 
     * Subtract two real numbers
     * 
     * @param a a real number
     * @param b a real number
     * @return the subtraction o -a- minus -b-
     */
   public double subtract (double a, double b) {
     double subtract;
     subtract = a - b;
     return subtract;
   }
   /** 
    * Divide two real numbers
    * 
    * @param a a real number
    * @param b a real numer
    * @return the division of a between b
    */
   public double divide (double a, double b) {
     return a / b;
   }
     
}   
