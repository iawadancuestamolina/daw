  /*
 *  Program. java         1.0 2/11/2020
 * 
 * Copyright 2020 Adán Cuesta Molina <1hiawcuestamolinaadan@gmail.com>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information
 */
 

import java.util.Scanner;

public class ProgramTUIprintfX {
    
    
   /**
   * translate farenheit degrees into celsius degrees
   * 
   *  @param a farenheit degrees
   *  @param b celsius degrees
   *  @return celsius degrees
   */
    
     public static void main(String[] args) {
        String nom, nom2;
        float pes, pes2;
        int edat, edat2;
        
                Scanner s = new Scanner(System.in);
                System.out.print("1) Com et dius?");
                //insert nom
                        nom = s.nextLine();
                        
                        System.out.println("Hola " + nom + "!!!");

                        System.out.print("1) Quina edat tens, " + nom + "?");
                //insert edat
                        edat = s.nextInt();
System.out.print("1) Quan peses?");
                //insert pes
                        pes = s.nextFloat();      
                        
                        //PERSONA 2
                           System.out.print("2) Com et dius?");
                //insert nom2
                        s.nextLine(); // Clean Buffer \n
nom2 = s.nextLine();
                        
                        System.out.println("Hola " + nom2 + "!!!");

                        System.out.print("2) Quina edat tens, " + nom2 + "?");
                //insert edat2
                        edat2 = s.nextInt();
System.out.print("2) Quan peses?");
                //insert pes2
                        pes2 = s.nextFloat();
                        
                        //show on screen
   System.out.println("-------------------------");
   System.out.printf("|%-11s:%11s|\n", "NOM", nom);
   System.out.println("-------------------------");
   System.out.printf("|%-11s:%11s|\n", "EDAT", edat);
   System.out.println("-------------------------");
   System.out.printf("|%-11s:%11s|\n", "PES", pes);
   System.out.println("-------------------------");
   System.out.printf("|%-11s:%11s|\n", "NOM2", nom2);
   System.out.println("-------------------------");
   System.out.printf("|%-11s:%11s|\n", "EDAT2", edat2);
   System.out.println("-------------------------");
   System.out.printf("|%-11s:%11s|\n", "PES2", pes2);
   System.out.println("-------------------------");

                        
     }
}
   
