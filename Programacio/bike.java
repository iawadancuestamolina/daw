public class Bicycle {


//ATRIBUTOS

//modelo de la bici
private String model;
//marcha delantero actual
private int frontSprocket;
//marcha trasero actual
private int rearSprocket;
//numero cambios delanteros
private final int nFrontSprockets;
//numero de cambios traseros
private final int nRearSprockets;
//velocidad
private double v;
//velocidad maxima
private final static int VMAX = 100;
//incremento de la velocidad
private final static int DV = 5;

//CONSTRUCTORES

public Bicycle() {
    this.model = "Mountain bike";
    this.frontSprocket = 3;
    this.rearSprocket = 1;
    this.nFrontSprockets = 3;
    this.nRearSprockets = 7;
    this.v = 0;
}

public Bicycle(int nFrontSprockets, int nRearSprockets, double v) {
    this.model = "Mountain bike";
    this.nFrontSpockets = nFrontSpockets;
    this.nRearSpockets = nRearSpockets;
    this.v = v;
}

public Bicycle(String model, int frontSprocket, int rearSprocket, int nFrontSprockets, int nRearSprockets, double v) {
    this.model = model;
    this.frontSprocket = frontSprocket;
    this.rearSprocket = rearSprocket;
    this.nFrontSprockets = nFrontSprockets;
    this.nRearSprockets = nRearSprockets;
    this.v = v;
}

//GETTERS AND SETTERS

public String getModel() {
    return model;
}

public void setModel(String model) {
    this.model = model.trim();
}

public int getRearSprocket() {
    return rearSprocket;
}

public void setRearSprocket(int rearSprocket) {
    this.rearSprocket = rearSprocket;
}

public void setFrontSprocket(int frontSprocket) {
    this.frontSprocket = frontSprocket;
}

public double getV() {
    return this.v;
}

public void setV(int v) {
    this.v = v;
}



//MÉTODOS

//metodo para cambiar de marcha delantera (coronas)
public boolean changeFrontSprocket(int n) {
    boolean isChanged = true;
    if (this.frontSprocket < this.nFrontSprockets && n > 0 && this.v > 0) {
        this.frontSprocket++;
    } else if (this.frontSprocket > 1 && n < 0 && this.v > 0) {
        this.frontSprocket--;
    } else {
        isChanged = false;
    }
    return isChanged;
}

//método para cambiar la marcha trasera (piñones)
public boolean changeRearSprocket(int n) {
    boolean isChanged = true;
    if (this.rearSprocket < this.nRearSprockets && n > 0 && this.v > 0) {
        this.rearSprocket++;
    } else if (this.rearSprocket > 1 && n < 0 && this.v > 0){
        this.rearSprocket--;
    } else {
        isChanged = false;
    }
    return isChanged;    
}

//método para acelerar
public void accelerate() {
    double newV = this.v + Bicycle.DV;
    if (newV > Bicycle.VMAX)
        newV = Bicycle.VMAX;
    this.v = newV;
}

//método para decelerar
public void brake() {
    double newV = this.v - Bicycle.DV;
    if (newV < 0)
        newV = 0;
    this.v = newV;
}




}