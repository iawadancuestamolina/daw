


--Esborrem la funció si existeix per evitar conflictes
drop function if exists prestecDocument(p_idUsuari prestec.idUsuari%type, p_titol document.titol%type, p_format varchar);

--Creem la funció
create or replace function prestecDocument(p_idUsuari prestec.idUsuari%type, p_titol document.titol%type, p_format varchar)
returns varchar
as $$
	declare
		v_usuari varchar(200);
		v_datadesbloqueig date;
		v_bloquejat boolean;

		v_missatge varchar default 'El prèstec s''ha fet amb èxit';
		v_idExemplar integer;
		v_diesBloqueig integer;
			-- constants pel tope de documents segons format
		maxRevistesLlibres constant integer := 2;
		maxCDs constant integer := 1;
		maxDVDs constant integer := 1;
		  -- vbles que emmagatzemaran la quantitat de prèstecs per tipus i usuari
		v_revistesLlibres integer;
		v_dvds integer;
		v_cds integer;
begin
				-- bloc comprovació usuari
	begin
		select nom || ' ' || cognoms usuari, dataDesbloqueig, bloquejat
		into strict v_usuari, v_datadesbloqueig, v_bloquejat
		from usuari
		where idUsuari=p_idUsuari;
	 exception
			when no_data_found then
			 	return 'Error: l''usuari ' || p_idUsuari || ' no existeix';
	end;   -- fi bloc comprovació usuari
	-- Comprovem que l'usuari no té el carnet bloquejat
if v_bloquejat then
   --OJO amb els nuls, si hi ha un NULL a dataDesbloqueig, la concatenació és NULL
  	v_missatge := 'Error: Usuari amb compte bloquejat fins el ' || coalesce(v_datadesbloqueig::varchar,'(data desconeguda)');
else
	/* Calcular per aquest usuari ACTUALMENT quants prèstecs té de cada tipus
	   Aquí he fet servir case, i sobre ell un sum, es podríen fer en 3 consultes
		 amb un count, però està fet així per tenir un codi més curt		 
  */
		select
				sum(case when lower(format)='cd' then 1 else 0 end) numCDs,
				sum (case when lower(format) in ('revista', 'llibre') then 1 else 0 end) numRevistesLlibres,
				sum (case when lower(format)='dvd' then 1 else 0 end) numDVDs,
				count (*) totalPrestecs
		into strict v_cds, v_revistesLlibres, v_dvds
		from exemplar e, document d, prestec p
    where d.idDocument = e.idDocument and p.idExemplar=e.idExemplar
							and datadev is null and idUsuari = p_idUsuari;
			-- Informem si l'usuari es passa del màxim de prèstecs simultanis
    case
			when lower(p_format) = 'cd' and v_cds = maxCDs then  -- constant
				v_missatge := 'Error: l''usuari '|| v_usuari||' té actualment en prèstec el màxim de CDs (' || v_cds || ')';
			when lower(p_format) = 'dvd' and v_dvds = maxDVDs then
		  	v_missatge := 'Error: l''usuari '|| v_usuari||' té actualment en prèstec el màxim de DVDs (' || v_dvds || ')';
		  when lower(p_format) in ('llibre','revista') and v_revistesLlibres = maxRevistesLlibres	then
  			v_missatge := 'Error: l''usuari '|| v_usuari||'  té actualment en prèstec el màxim de llibre i revistes (' || v_revistesLlibres || ')';
			-- Comprovem la disponibilitat del document
			else
			  v_idExemplar := codiExemplarDisponible(p_titol);
				if v_idExemplar = 0 then
					v_missatge := 'El document que vols llogar no està disponible';
				else
				-- Si arribem aquí, vol dir que s'han superat els 3 requeriments
				  insert into prestec
				  values (v_idExemplar, current_timestamp, null, p_idUsuari);
				end if; -- Comprovació exemplar disponible
		end case;   -- Comprovació de limit de prèstecs segons tipus
	end if;       -- Comprovació usuari bloquejat/no bloquejat
	return v_missatge;
end;
$$ language plpgsql;
