--Años, meses y días que llevan contratados los empleados
select ename, 
extract(years from age(hiredate)) "años contratados", 
extract(months from age(hiredate)) "meses contratados", 
extract(days from age(hiredate)) "días contratados" 
from emp;

--funcion helloWorld
CREATE OR REPLACE FUNCTION helloWorld(   )
RETURNS VARCHAR
as $$
      -- noteu que no té secció DECLARE, perquè no tenim variables locals
  BEGIN  
      -- el tipus de dades que retorna ha de coincidir amb el declarat a la capçalera de la funció
      return 'Hello world!';  
  END;
$$ LANGUAGE plpgsql;

--ejecutar la función
select helloWorld();

--función añadir IVA
CREATE OR REPLACE FUNCTION aplicarIVA(preu_sense_iva    numeric)
RETURNS numeric
 as $$
 DECLARE
      iva numeric(6,2); -- variable local 9999,99
BEGIN
iva := preu_sense_iva * 21 / 100;
return preu_sense_iva + iva;  
END;
$$ LANGUAGE plpgsql;

--seleccionar todos los objetos en todos los esquemas de mi usuario
select relname, nspname, r.rolname 
from pg_class c join pg_roles r on c.relowner=r.oid 
join pg_namespace n on c.relnamespace=n.oid 
where lower(r.rolname)='iaw50482126';

--deberes case
select relname,                                                    
case relkind when 'i' then 'index'
        when 's' then 'sequence'
        when 'r' then 'table'
        else 'other'
        end, nspname, r.rolname 
from pg_class c join pg_roles r on c.relowner=r.oid 
join pg_namespace n on c.relnamespace=n.oid 
where lower(r.rolname)='iaw50482126';

--ejercicio 30/4 aplicar descuento al empleado en función del departamento
--dept 10 - 15% , 20 - 25% , 30 - 35%, else 45%
do $$
DECLARE
    preu int;
    dept int;
    descompte int;
BEGIN 
    if dept = 10 then 
        descompte := 15;
    else 
        if dept = 20 then
            descompte := 25;
        else
            if dept = 30 then
                descompte := 35;
            else 
                descompte := 45;
            end if;
        end if;
    end if;

    raise info 'Descompte de %', descompte;
    end $$;
    -- versión elsif

    --funcion de un dia x ahi
    CREATE OR REPLACE FUNCTION test()
RETURNS varchar
 as $$
    DECLARE
        v_ename varchar(30);
        begin
        select ename
        into strict v_ename
        from emp
where empno=7499;
return 'El nom del empleat es: ' || v_ename;
exception when NO_DATA_FOUND then
return'Error: No s"ha trobat el nom';
end;
$$ LANGUAGE plpgsql;

--crear funcion retornaDept() en la que con un nom de empleat retorna el seu departament ::text
    CREATE OR REPLACE FUNCTION retornaDept(nombre varchar)
RETURNS varchar
 as $$
    DECLARE
        v_depart varchar(30);
        begin
        select dname
        into strict v_depart
        from emp e join dept d on e.deptno=d.deptno
where lower(e.ename)=lower(nombre);
return 'El departament del empleat es: ' || v_depart;
exception when NO_DATA_FOUND then
return'Error: No s"ha trobat el departament';
end;
$$ LANGUAGE plpgsql;