\c template1 
drop database if exists habexam;
create database habexam;
\c habexam

create table municipi (
    idMunicipi smallint not null,
    nom varchar(40),
    coordenades varchar(50),
    constraint pk_municipi_idMunicipi primary key (idMunicipi)
);

create table habitatge (
    codiCatastral smallint not null,
    direccion varchar(50),
    m2 smallint,
    idMunicipi smallint,
    constraint pk_habitatge_codiCatastral primary key (codiCatastral),
    constraint fk_habitatge_idMunicipi foreign key (idMunicipi) references municipi(idMunicipi)
);

create table persona (
    dni varchar(9) not null,
    nom varchar(50),
    telefon smallint,
    sexe varchar(10),
    edat smallint,
    codiCatastral smallint,
    capFam varchar(9),
    constraint pk_persona_dni primary key (dni),
    constraint fk_persona_codiCatastral foreign key (codiCatastral) references habitatge (codiCatastral)
);

alter table persona add constraint fk_persona_capFam foreign key (capFam) references persona(dni);

create table propietat (
    dni varchar(9) not null,
    codiCatastral smallint not null,
    constraint pk_propietat_dni_codiCatastral primary key (dni,codiCatastral),
    constraint fk_propietat_dni foreign key (dni) references persona(dni),
    constraint fk_propietat_codiCatastral foreign key (codiCatastral) references habitatge(codiCatastral)
);

