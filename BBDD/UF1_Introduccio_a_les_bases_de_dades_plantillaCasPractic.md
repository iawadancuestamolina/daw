# Plantilla per implementar els casos pràctics

- Per cada cas pràctic s'ha de lliurar un fitxer en format markdown, amb el número d'ordre, guió baix (\_) i el nom del cas.
P.e.: `6_zoos.md`

- L'esquema conceptual (entitat-relació) s'ha de fer amb [draw.io](https://www.draw.io/)). Haureu de generar un enllaç públic i l'esquema exportat a format png.

- El contingut del fitxer ha de seguir la següent estructura numerada de sota. `Esborreu d'aquí (inclòs) cap a dalt quan feu l'entrega`

# 1. 11\Club Nautic

Es vol dissenyar una base de dades relacional per a gestionar les dades dels socis d'un club nàutic. De cada soci es guarden les dades personals i les dades del vaixell o vaixells que posseeix: número de matrícula, nom, número de l'amarratge i quota que paga pel mateix. A més, es vol mantenir informació sobre les sortides realitzades per cada vaixell, com la data i hora de sortida, el destí i les dades personals del patró, que no ha de ser el propietari del vaixell, ni cal que sigui soci del club.

# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[cas 11\Club\Nautic](http://...)
## 2.2. Esquema conceptual (EC ó ER)
  ![cas 11\Club\Nautic](https://ibb.co/Jt9fv9p)
# 3. Model lògic relacional
## 3.1. Esquema lògic
  Relacio1(<ins>idXX</ins>, atribut1, atribut1, ...)  
  Relacio2(<ins>idZZ</ins>, atribut1, atribut2, ...)
  \...

## 3.2. Diagrama referencial

Relació referencial|Clau aliena|Relació referida
-|:-:|-
x|x|x
x|x|x

# 4. Model físic
## 4.1 Enllaç a l'esquema físic

[script xxx.sql](./path/to/script.sql)
